voor_1_januari_2013(Peildatum, UsageRef, RefsTree)
    :-
       RefsTree  = node(
           fact("voor_1_januari_2013", UsageRef),
           operator(voor_datum, ref(["art7a1a_id2"])),
           children([OperatorTree])
       ),
       datum_voor_datum(Peildatum, leaf(datum(2013,1,1), ref(["art7a1a_id1"])),
                        ref(["art7a1a_id2"]),
                        OperatorTree)
    .

leeftijd_van_65_jaar(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_65_jaar", UsageRef),
            operator("leeftijd", ref(["art7a1_pl"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(65,0), ref(["art7a1a_id3"]))
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_voor_1_januari_2013(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_65_jaar_voor_1_januari_2013", UsageRef),
            operator("and", ref(["art7a1a_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        voor_1_januari_2013(Peildatum,
                            ref(["art7a1a_id8"]),
                            Operator1RefsTree),
        leeftijd_van_65_jaar(Leeftijd,
                             ref(["art7a1a_id4"]),
                             Operator2RefsTree)
    .

in_2013(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2013", UsageRef),
            operator("in_jaar", ref(["art7a1b_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2013), ref(["art7a1b_id1"])),
                      ref(["art7a1b_id2"]),
                      OperatorTree)
    .

leeftijd_van_65_jaar_en_1_maand(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_65_jaar_en_1_maand", UsageRef),
            operator("leeftijd", ref(["art7a1_pl"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(65,1), ref(["art7a1b_id3"]))
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_en_1_maand_in_2013(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_65_jaar_en_1_maand_in_2013", UsageRef),
            operator("and", ref(["art7a1b_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2013(Peildatum,
                ref(["art7a1b_id8"]),
                Operator1RefsTree),
        leeftijd_van_65_jaar_en_1_maand(Leeftijd,
                                        ref(["art7a1b_id4"]),
                                        Operator2RefsTree)
    .

in_2014(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2014", UsageRef),
            operator("in_jaar", ref(["art7a1c_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2014), ref(["art7a1c_id1"])),
                      ref(["art7a1c_id2"]),
                      OperatorTree)
    .

leeftijd_van_65_jaar_en_2_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_65_jaar_en_2_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_pl"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(65,2), ref(["art7a1c_id3"]))
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_en_2_maanden_in_2014(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_65_jaar_en_2_maanden_in_2014", UsageRef),
            operator("and", ref(["art7a1c_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2014(Peildatum,
                ref(["art7a1c_id8"]),
                Operator1RefsTree),
        leeftijd_van_65_jaar_en_2_maanden(Leeftijd,
                                          ref(["art7a1c_id4"]),
                                          Operator2RefsTree)
    .

in_2015(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2015", UsageRef),
            operator("in_jaar", ref(["art7a1d_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2015), ref(["art7a1d_id1"])),
                      ref(["art7a1d_id2"]),
                      OperatorTree)
    .

leeftijd_van_65_jaar_en_3_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_65_jaar_en_3_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_pl"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(65,3), ref(["art7a1d_id3"]))
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_en_3_maanden_in_2015(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_65_jaar_en_3_maanden_in_2015", UsageRef),
            operator("and", ref(["art7a1d_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2015(Peildatum,
                ref(["art7a1d_id8"]),
                Operator1RefsTree),
        leeftijd_van_65_jaar_en_3_maanden(Leeftijd,
                                          ref(["art7a1d_id4"]),
                                          Operator2RefsTree)
    .

in_2016(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2016", UsageRef),
            operator("in_jaar", ref(["art7a1e_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2016), ref(["art7a1e_id1"])),
                      ref(["art7a1e_id2"]),
                      OperatorTree)
    .

leeftijd_van_65_jaar_en_6_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_65_jaar_en_6_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_pl"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(65,6), ref(["art7a1e_id3"]))
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_en_6_maanden_in_2016(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_65_jaar_en_6_maanden_in_2016", UsageRef),
            operator("and", ref(["art7a1e_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2016(Peildatum,
                ref(["art7a1e_id8"]),
                Operator1RefsTree),
        leeftijd_van_65_jaar_en_6_maanden(Leeftijd,
                                          ref(["art7a1e_id4"]),
                                          Operator2RefsTree)
    .

in_2017(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2017", UsageRef),
            operator("in_jaar", ref(["art7a1f_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2017), ref(["art7a1f_id1"])),
                      ref(["art7a1f_id2"]),
                      OperatorTree)
    .

leeftijd_van_65_jaar_en_9_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_65_jaar_en_9_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_pl"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(65,9), ref(["art7a1f_id3"]))
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_en_9_maanden_in_2017(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_65_jaar_en_9_maanden_in_2017", UsageRef),
            operator("and", ref(["art7a1f_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2017(Peildatum,
                ref(["art7a1f_id8"]),
                Operator1RefsTree),
        leeftijd_van_65_jaar_en_9_maanden(Leeftijd,
                                          ref(["art7a1f_id4"]),
                                          Operator2RefsTree)
    .

in_2018(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2018", UsageRef),
            operator("in_jaar", ref(["art7a1g_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2018), ref(["art7a1g_id1"])),
                      ref(["art7a1g_id2"]),
                      OperatorTree)
    .

leeftijd_van_66_jaar(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_66_jaar", UsageRef),
            operator("leeftijd", ref(["art7a1_pl"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(66,0), ref(["art7a1g_id3"]))
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_in_2018(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_66_jaar_in_2018", UsageRef),
            operator("and", ref(["art7a1g_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2018(Peildatum,
                ref(["art7a1g_id8"]),
                Operator1RefsTree),
        leeftijd_van_66_jaar(Leeftijd,
                             ref(["art7a1g_id4"]),
                             Operator2RefsTree)
    .

in_2019(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2019", UsageRef),
            operator("in_jaar", ref(["art7a1h_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2019), ref(["art7a1h_id1"])),
                      ref(["art7a1h_id2"]),
                      OperatorTree)
    .

leeftijd_van_66_jaar_en_4_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_66_jaar_en_4_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_pl"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(66,4), ref(["art7a1h_id3", "art7a1i_id3", "art7a1j_id3"]))
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2019(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2019", UsageRef),
            operator("and", ref(["art7a1h_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2019(Peildatum,
                ref(["art7a1h_id8"]),
                Operator1RefsTree),
        leeftijd_van_66_jaar_en_4_maanden(Leeftijd,
                                          ref(["art7a1h_id4"]),
                                          Operator2RefsTree)
    .

in_2020(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2020", UsageRef),
            operator("in_jaar", ref(["art7a1i_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2020), ref(["art7a1i_id1"])),
                      ref(["art7a1i_id2"]),
                      OperatorTree)
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2020(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2020", UsageRef),
            operator("and", ref(["art7a1i_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2020(Peildatum,
                ref(["art7a1i_id8"]),
                Operator1RefsTree),
        leeftijd_van_66_jaar_en_4_maanden(Leeftijd,
                                          ref(["art7a1i_id4"]),
                                          Operator2RefsTree)
    .

in_2021(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2021", UsageRef),
            operator("in_jaar", ref(["art7a1j_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2021), ref(["art7a1j_id1"])),
                      ref(["art7a1j_id2"]),
                      OperatorTree)
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2021(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2021", UsageRef),
            operator("and", ref(["art7a1j_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2021(Peildatum,
                ref(["art7a1j_id8"]),
                Operator1RefsTree),
        leeftijd_van_66_jaar_en_4_maanden(Leeftijd,
                                          ref(["art7a1j_id4"]),
                                          Operator2RefsTree)
    .

in_2022(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2022", UsageRef),
            operator("in_jaar", ref(["art7a1k_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2022), ref(["art7a1k_id1"])),
                      ref(["art7a1k_id2"]),
                      OperatorTree)
    .

leeftijd_van_66_jaar_en_7_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_66_jaar_en_7_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_pl"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(66,7), ref(["art7a1k_id3"]))
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_en_7_maanden_in_2022(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_66_jaar_en_7_maanden_in_2022", UsageRef),
            operator("and", ref(["art7a1k_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2022(Peildatum,
                ref(["art7a1k_id8"]),
                Operator1RefsTree),
        leeftijd_van_66_jaar_en_7_maanden(Leeftijd,
                                          ref(["art7a1k_id4"]),
                                          Operator2RefsTree)
    .

in_2023(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2023", UsageRef),
            operator("in_jaar", ref(["art7a1l_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2023), ref(["art7a1l_id1"])),
                      ref(["art7a1l_id2"]),
                      OperatorTree)
    .

leeftijd_van_66_jaar_en_10_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_66_jaar_en_10_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_pl"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(66,10), ref(["art7a1l_id3"]))
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_en_10_maanden_in_2023(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_66_jaar_en_10_maanden_in_2023", UsageRef),
            operator("and", ref(["art7a1l_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2023(Peildatum,
                ref(["art7a1l_id8"]),
                Operator1RefsTree),
        leeftijd_van_66_jaar_en_10_maanden(Leeftijd,
                                           ref(["art7a1l_id4"]),
                                           Operator2RefsTree)
    .

in_2024(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2024", UsageRef),
            operator("in_jaar", ref(["art7a1m_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2024), ref(["art7a1m_id1"])),
                      ref(["art7a1m_id2"]),
                      OperatorTree)
    .

leeftijd_van_67_jaar(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_67_jaar", UsageRef),
            operator("leeftijd", ref(["art7a1_pl"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(67,0), ref(["art7a1m_id3", "art7a1n_id3"]))
    .

de_pensioengerechtigde_leeftijd_van_67_jaar_in_2024(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_67_jaar_in_2024", UsageRef),
            operator("and", ref(["art7a1m_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2024(Peildatum,
                ref(["art7a1m_id8"]),
                Operator1RefsTree),
        leeftijd_van_67_jaar(Leeftijd,
                             ref(["art7a1m_id4"]),
                             Operator2RefsTree)
    .

in_2025(Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("in_2025", UsageRef),
            operator("in_jaar", ref(["art7a1n_id2"])),
            children([OperatorTree])
        ),
        datum_in_jaar(Peildatum, leaf(jaar(2025), ref(["art7a1n_id1"])),
                      ref(["art7a1n_id2"]),
                      OperatorTree)
    .

de_pensioengerechtigde_leeftijd_van_67_jaar_in_2025(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd_van_67_jaar_in_2025", UsageRef),
            operator("and", ref(["art7a1n_id7"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2025(Peildatum,
                ref(["art7a1n_id8"]),
                Operator1RefsTree),
        leeftijd_van_67_jaar(Leeftijd,
                             ref(["art7a1n_id4"]),
                             Operator2RefsTree)
    .

de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator1RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_65_jaar_voor_1_januari_2013(Leeftijd, Peildatum,
                                                                        ref(["art7a1_or1a"]),
                                                                        Operator1RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator2RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_65_jaar_en_1_maand_in_2013(Leeftijd, Peildatum,
                                                                       ref(["art7a1_or1b"]),
                                                                       Operator2RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator3RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_65_jaar_en_2_maanden_in_2014(Leeftijd, Peildatum,
                                                                         ref(["art7a1_or1c"]),
                                                                         Operator3RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator4RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_65_jaar_en_3_maanden_in_2015(Leeftijd, Peildatum,
                                                                         ref(["art7a1_or1d"]),
                                                                         Operator4RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator5RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_65_jaar_en_6_maanden_in_2016(Leeftijd, Peildatum,
                                                                         ref(["art7a1_or1e"]),
                                                                         Operator5RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator6RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_65_jaar_en_9_maanden_in_2017(Leeftijd, Peildatum,
                                                                         ref(["art7a1_or1f"]),
                                                                         Operator6RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator7RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_66_jaar_in_2018(Leeftijd, Peildatum,
                                                            ref(["art7a1_or1g"]),
                                                            Operator7RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator8RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2019(Leeftijd, Peildatum,
                                                                         ref(["art7a1_or1h"]),
                                                                         Operator8RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator9RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2020(Leeftijd, Peildatum,
                                                                         ref(["art7a1_or1i"]),
                                                                         Operator9RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator10RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2021(Leeftijd, Peildatum,
                                                                         ref(["art7a1_or1j"]),
                                                                         Operator10RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator11RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_66_jaar_en_7_maanden_in_2022(Leeftijd, Peildatum,
                                                                         ref(["art7a1_or1k"]),
                                                                         Operator11RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator12RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_66_jaar_en_10_maanden_in_2023(Leeftijd, Peildatum,
                                                                          ref(["art7a1_or1l"]),
                                                                          Operator12RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator13RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_67_jaar_in_2024(Leeftijd, Peildatum,
                                                            ref(["art7a1_or1m"]),
                                                            Operator13RefsTree)
    .
de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_pensioengerechtigde_leeftijd", UsageRef),
            operator("or", ref(["art7a1_id2"])),
            children([Operator14RefsTree])
        ),
        de_pensioengerechtigde_leeftijd_van_67_jaar_in_2025(Leeftijd, Peildatum,
                                                            ref(["art7a1_or1n"]),
                                                            Operator14RefsTree)
    .

bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("bereikt_de_pensioengerechtigde_leeftijd", UsageRef),
            operator("reaches", ref(["art7a1_rav"])),
            children([TeBereikenRT, HeeftLeeftijdRT, DagErvoorRT, LeeftijdsvoorwaardeDagErvoorRT, DagErvoorNogNietBereiktRT])
        ),
        % huidige leeftijdsvoorwaarde
        de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum,
                                        ref(["art7a1_id5"]),
                                        TeBereikenRT),
        % huidige leeftijdsvoorwaarde bereikt
        heeft_leeftijd_op_datum(bereikt, Geboortedatum, Leeftijd, Peildatum,
                                implication("hetgeen te bereiken is een leeftijd"),
                                HeeftLeeftijdRT),
        % dag ervoor
        dag_voor_datum(_datumDagVoorPeildatum, Peildatum,
                       implication("een leeftijd wordt bereikt op de dag waarop deze voor het eerst bereikt is"),
                       DagErvoorRT),
        % leeftijdsvoorwaarde van dag ervoor
        de_pensioengerechtigde_leeftijd(_leeftijdsvoorwaardeDagVoorPeildatum, _datumDagVoorPeildatum,
                                        implication("de geldende te bereiken leeftijd op de dag voordat de leeftijd wordt bereikt"),
                                        LeeftijdsvoorwaardeDagErvoorRT),
        % leeftijdsvoorwaarde nog niet bereikt dag ervoor
        heeft_leeftijd_op_datum(nog_niet_bereikt, Geboortedatum, _leeftijdsvoorwaardeDagVoorPeildatum, _datumDagVoorPeildatum,
                                implication("op de dag voordat de leeftijd wordt bereikt is de leeftijd nog niet bereikt"),
                                DagErvoorNogNietBereiktRT),
        % leeftijd wordt voor het eerst bereikt
        % TODO negatie (\+) kan geen referentie krijgen omdat het voor geen enkele
        % referentie waar is. Om dit toch inzichtelijk te maken kan ipv daarvan een
        % referentie gegeven worden voor elke dag voordat de leeftijdsvoorwaarde
        % veranderd.  Bij de pensioengerechtigde leeftijd kan dan bijvoorbeeld duidelijk
        % worden dat iemand de leeftijd niet eerder heeft bereikt door voor elk jaar te
        % laten zien dat de maximale leeftijd (die op 31-12) kleiner is dan de in dat jaar
        % geldende pensioengerechtigde leeftijd.
        \+ (datum_voor_datum(_nogEerderePeildatum, _datumDagVoorPeildatum, _, _),
            de_pensioengerechtigde_leeftijd(_nogEerdereLeeftijdsvoorwaarde, _nogEerderePeildatum, _, _),
            heeft_leeftijd_op_datum(bereikt, Geboortedatum, _nogEerdereLeeftijdsvoorwaarde, _nogEerderePeildatum, _, _)
            )
    .

persoonlijke_pensioengerechtigde_leeftijd(Geboortedatum, Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("persoonlijke_pensioengerechtigde_leeftijd", UsageRef),
            operator("and", ref(["art7a1_id3"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, Peildatum,
                                                ref(["art7a1_id4"]),
                                                Operator1RefsTree),
        de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum,
                                        ref(["art7a1_id7"]),
                                        Operator2RefsTree)
    .

leeftijd_van_15_jaar(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_15_jaar", UsageRef),
            operator("leeftijd", ref(["art7a1_al"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(15,0), ref(["art7a1a_id5"]))
    .

de_aanvangsleeftijd_van_15_jaar_voor_1_januari_2013(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_15_jaar_voor_1_januari_2013", UsageRef),
            operator("and", ref(["art7a1a_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        voor_1_januari_2013(Peildatum,
                            ref(["art7a1a_id8"]),
                            Operator1RefsTree),
        leeftijd_van_15_jaar(Leeftijd,
                             ref(["art7a1a_id6"]),
                             Operator2RefsTree)
    .

leeftijd_van_15_jaar_en_1_maand(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_15_jaar_en_1_maand", UsageRef),
            operator("leeftijd", ref(["art7a1_al"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(15,1), ref(["art7a1b_id5"]))
    .

de_aanvangsleeftijd_van_15_jaar_en_1_maand_in_2013(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_15_jaar_en_1_maand_in_2013", UsageRef),
            operator("and", ref(["art7a1b_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2013(Peildatum,
                ref(["art7a1b_id8"]),
                Operator1RefsTree),
        leeftijd_van_15_jaar_en_1_maand(Leeftijd,
                                        ref(["art7a1b_id6"]),
                                        Operator2RefsTree)
    .

leeftijd_van_15_jaar_en_2_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_15_jaar_en_2_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_al"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(15,2), ref(["art7a1c_id5"]))
    .

de_aanvangsleeftijd_van_15_jaar_en_2_maanden_in_2014(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_15_jaar_en_2_maanden_in_2014", UsageRef),
            operator("and", ref(["art7a1c_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2014(Peildatum,
                ref(["art7a1c_id8"]),
                Operator1RefsTree),
        leeftijd_van_15_jaar_en_2_maanden(Leeftijd,
                                          ref(["art7a1c_id6"]),
                                          Operator2RefsTree)
    .

leeftijd_van_15_jaar_en_3_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_15_jaar_en_3_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_al"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(15,3), ref(["art7a1d_id5"]))
    .

de_aanvangsleeftijd_van_15_jaar_en_3_maanden_in_2015(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_15_jaar_en_3_maanden_in_2015", UsageRef),
            operator("and", ref(["art7a1d_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2015(Peildatum,
                ref(["art7a1d_id8"]),
                Operator1RefsTree),
        leeftijd_van_15_jaar_en_3_maanden(Leeftijd,
                                          ref(["art7a1d_id6"]),
                                          Operator2RefsTree)
    .

leeftijd_van_15_jaar_en_6_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_15_jaar_en_6_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_al"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(15,6), ref(["art7a1e_id5"]))
    .

de_aanvangsleeftijd_van_15_jaar_en_6_maanden_in_2016(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_15_jaar_en_6_maanden_in_2016", UsageRef),
            operator("and", ref(["art7a1e_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2016(Peildatum,
                ref(["art7a1e_id8"]),
                Operator1RefsTree),
        leeftijd_van_15_jaar_en_6_maanden(Leeftijd,
                                          ref(["art7a1e_id6"]),
                                          Operator2RefsTree)
    .

leeftijd_van_15_jaar_en_9_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_15_jaar_en_9_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_al"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(15,9), ref(["art7a1f_id5"]))
    .

de_aanvangsleeftijd_van_15_jaar_en_9_maanden_in_2017(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_15_jaar_en_9_maanden_in_2017", UsageRef),
            operator("and", ref(["art7a1f_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2017(Peildatum,
                ref(["art7a1f_id8"]),
                Operator1RefsTree),
        leeftijd_van_15_jaar_en_9_maanden(Leeftijd,
                                          ref(["art7a1f_id6"]),
                                          Operator2RefsTree)
    .

leeftijd_van_16_jaar(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_16_jaar", UsageRef),
            operator("leeftijd", ref(["art7a1_al"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(16,0), ref(["art7a1g_id5"]))
    .

de_aanvangsleeftijd_van_16_jaar_in_2018(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_16_jaar_in_2018", UsageRef),
            operator("and", ref(["art7a1g_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2018(Peildatum,
                ref(["art7a1g_id8"]),
                Operator1RefsTree),
        leeftijd_van_16_jaar(Leeftijd,
                             ref(["art7a1g_id6"]),
                             Operator2RefsTree)
    .

leeftijd_van_16_jaar_en_4_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_16_jaar_en_4_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_al"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(16,4), ref(["art7a1h_id5", "art7a1i_id5", "art7a1j_id5"]))
    .

de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2019(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2019", UsageRef),
            operator("and", ref(["art7a1h_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2019(Peildatum,
                ref(["art7a1h_id8"]),
                Operator1RefsTree),
        leeftijd_van_16_jaar_en_4_maanden(Leeftijd,
                                          ref(["art7a1h_id6"]),
                                          Operator2RefsTree)
    .

de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2020(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2020", UsageRef),
            operator("and", ref(["art7a1i_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2020(Peildatum,
                ref(["art7a1i_id8"]),
                Operator1RefsTree),
        leeftijd_van_16_jaar_en_4_maanden(Leeftijd,
                                          ref(["art7a1i_id6"]),
                                          Operator2RefsTree)
    .

de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2021(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2021", UsageRef),
            operator("and", ref(["art7a1j_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2021(Peildatum,
                ref(["art7a1j_id8"]),
                Operator1RefsTree),
        leeftijd_van_16_jaar_en_4_maanden(Leeftijd,
                                          ref(["art7a1j_id6"]),
                                          Operator2RefsTree)
    .

leeftijd_van_16_jaar_en_7_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_16_jaar_en_7_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_al"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(16,7), ref(["art7a1k_id5"]))
    .

de_aanvangsleeftijd_van_16_jaar_en_7_maanden_in_2022(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_16_jaar_en_7_maanden_in_2022", UsageRef),
            operator("and", ref(["art7a1k_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2022(Peildatum,
                ref(["art7a1k_id8"]),
                Operator1RefsTree),
        leeftijd_van_16_jaar_en_7_maanden(Leeftijd,
                                          ref(["art7a1k_id6"]),
                                          Operator2RefsTree)
    .

leeftijd_van_16_jaar_en_10_maanden(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_16_jaar_en_10_maanden", UsageRef),
            operator("leeftijd", ref(["art7a1_al"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(16,10), ref(["art7a1l_id5"]))
    .

de_aanvangsleeftijd_van_16_jaar_en_10_maanden_in_2023(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_16_jaar_en_10_maanden_in_2023", UsageRef),
            operator("and", ref(["art7a1l_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2023(Peildatum,
                ref(["art7a1l_id8"]),
                Operator1RefsTree),
        leeftijd_van_16_jaar_en_10_maanden(Leeftijd,
                                           ref(["art7a1l_id6"]),
                                           Operator2RefsTree)
    .

leeftijd_van_17_jaar(Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("leeftijd_van_17_jaar", UsageRef),
            operator("leeftijd", ref(["art7a1_al"])),
            children([Leeftijd])
        ),
        Leeftijd = leaf(leeftijd(17,0), ref(["art7a1m_id5", "art7a1n_id5"]))
    .

de_aanvangsleeftijd_van_17_jaar_in_2024(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_17_jaar_in_2024", UsageRef),
            operator("and", ref(["art7a1m_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2024(Peildatum,
                ref(["art7a1m_id8"]),
                Operator1RefsTree),
        leeftijd_van_17_jaar(Leeftijd,
                             ref(["art7a1m_id6"]),
                             Operator2RefsTree)
    .

de_aanvangsleeftijd_van_17_jaar_in_2025(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd_van_17_jaar_in_2025", UsageRef),
            operator("and", ref(["art7a1n_id9"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        in_2025(Peildatum,
                ref(["art7a1n_id8"]),
                Operator1RefsTree),
        leeftijd_van_17_jaar(Leeftijd,
                             ref(["art7a1n_id6"]),
                             Operator2RefsTree)
    .

de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator1RefsTree])
        ),
        de_aanvangsleeftijd_van_15_jaar_voor_1_januari_2013(Leeftijd, Peildatum,
                                                            ref(["art7a1_or2a"]),
                                                            Operator1RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator2RefsTree])
        ),
        de_aanvangsleeftijd_van_15_jaar_en_1_maand_in_2013(Leeftijd, Peildatum,
                                                           ref(["art7a1_or2b"]),
                                                           Operator2RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator3RefsTree])
        ),
        de_aanvangsleeftijd_van_15_jaar_en_2_maanden_in_2014(Leeftijd, Peildatum,
                                                             ref(["art7a1_or2c"]),
                                                             Operator3RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator4RefsTree])
        ),
        de_aanvangsleeftijd_van_15_jaar_en_3_maanden_in_2015(Leeftijd, Peildatum,
                                                             ref(["art7a1_or2d"]),
                                                             Operator4RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator5RefsTree])
        ),
        de_aanvangsleeftijd_van_15_jaar_en_6_maanden_in_2016(Leeftijd, Peildatum,
                                                             ref(["art7a1_or2e"]),
                                                             Operator5RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator6RefsTree])
        ),
        de_aanvangsleeftijd_van_15_jaar_en_9_maanden_in_2017(Leeftijd, Peildatum,
                                                             ref(["art7a1_or2f"]),
                                                             Operator6RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator7RefsTree])
        ),
        de_aanvangsleeftijd_van_16_jaar_in_2018(Leeftijd, Peildatum,
                                                ref(["art7a1_or2g"]),
                                                Operator7RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator8RefsTree])
        ),
        de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2019(Leeftijd, Peildatum,
                                                             ref(["art7a1_or2h"]),
                                                             Operator8RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator9RefsTree])
        ),
        de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2020(Leeftijd, Peildatum,
                                                             ref(["art7a1_or2i"]),
                                                             Operator9RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator10RefsTree])
        ),
        de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2021(Leeftijd, Peildatum,
                                                             ref(["art7a1_or2j"]),
                                                             Operator10RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator11RefsTree])
        ),
        de_aanvangsleeftijd_van_16_jaar_en_7_maanden_in_2022(Leeftijd, Peildatum,
                                                             ref(["art7a1_or2k"]),
                                                             Operator11RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator12RefsTree])
        ),
        de_aanvangsleeftijd_van_16_jaar_en_10_maanden_in_2023(Leeftijd, Peildatum,
                                                              ref(["art7a1_or2l"]),
                                                              Operator12RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator13RefsTree])
        ),
        de_aanvangsleeftijd_van_17_jaar_in_2024(Leeftijd, Peildatum,
                                                ref(["art7a1_or2m"]),
                                                Operator13RefsTree)
    .
de_aanvangsleeftijd(Leeftijd, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("de_aanvangsleeftijd", UsageRef),
            operator("or", ref(["art7a1_id6"])),
            children([Operator14RefsTree])
        ),
        de_aanvangsleeftijd_van_17_jaar_in_2025(Leeftijd, Peildatum,
                                                ref(["art7a1_or2n"]),
                                                Operator14RefsTree)
    .

persoonlijke_aanvangsleeftijd(Geboortedatum, Leeftijd, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("persoonlijke_aanvangsleeftijd", UsageRef),
            operator("and", ref(["art7a1_id3"])),
            children([Operator1RefsTree, Operator2RefsTree])
        ),
        bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, Peildatum,
                                                ref(["art7a1_id4"]),
                                                Operator1RefsTree),
        de_aanvangsleeftijd(Leeftijd, Peildatum,
                            ref(["art7a1_id8"]),
                            Operator2RefsTree)
    .

