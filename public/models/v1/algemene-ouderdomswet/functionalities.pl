% -*- Prolog -*-

notificatie(Vandaag, Geboortedatum, DagenTotPensioen, "U gaat over minder dan een maand met pensioen! Bekijk nu in de PRA de dingen om op te letten.") :-
    bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, Pensioendatum),
    DagenTotPensioen = dagen(Restdagen),
    Restdagen #>= 1, % Er zijn nog restdagen in het verschil tussen vandaag en de pensioendatum
    datum_datum_maandenverschil_restdagen(Vandaag, Pensioendatum, maanden(0), dagen(Restdagen)). % verschil is 0 maanden

notificatie(Vandaag, Geboortedatum, DagenTotPensioen, Bericht, ThisTree) :-
    bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, Pensioendatum,
        reason("nodig voor de notificatie functionaliteit"),
        PensioenGerechtigdTree),
    Restdagen #>= 1, % Er zijn nog restdagen in het verschil tussen vandaag en de pensioendatum
    DagenTotPensioen = leaf(dagen(Restdagen), _),
    Vandaag = leaf(VandaagDtm, _),
    Pensioendatum = leaf(PensioendatumDtm, _),
    datum_datum_maandenverschil_restdagen(VandaagDtm, PensioendatumDtm, maanden(0), dagen(Restdagen)), % verschil is 0 maanden
    Bericht = leaf(message("U gaat over minder dan een maand met pensioen! Bekijk nu in de PRA de dingen om op te letten."), _),
    ThisTree = node(
        app(ref(["besluit_id1"])),
        operator("notificatie", reason("*Burgers worden geinformeerd wanneer ze over minder dan een maand met pensioen gaan*")),
        children([PensioenGerechtigdTree])
    ).

wordt_pensioengerechtigd_bij_leeftijd_op_datum(Geboortedatum, Pensioenleeftijd, Pensioendatum) :-
    bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, Pensioendatum),
    persoonlijke_pensioengerechtigde_leeftijd(Geboortedatum, Pensioenleeftijd).

wordt_pensioengerechtigd_bij_leeftijd_op_datum(Geboortedatum, _, Pensioendatum, PensioenGerechtigdTree) :-
    bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, Pensioendatum,
        calculation(
            expr("Geboortedatum"), txt("bereikt de pensioengerechtigde leeftijd op"), expr("Datum"),
            reason("nodig voor de wordt_pensioengerechtigd_bij_leeftijd_op_datum functionaliteit")),
        PensioenGerechtigdTree).

func_persoonlijke_aanvangsleeftijd(Geboortedatum, Leeftijd, RefsTree) :-
    Leeftijd = leaf(L,_),
    _leeftijd = leaf(L,_),
    persoonlijke_aanvangsleeftijd(Geboortedatum, _leeftijd,
        calculation(
            expr("Geboortedatum"), txt("heeft de aanvangsleeftijd van"), expr("Leeftijd"),
            reason("nodig om de pensioengerechtigdheid te bepalen")),
        RefsTree).

func_persoonlijke_pensioengerechtigde_leeftijd(Geboortedatum, Leeftijd, RefsTree) :-
    Leeftijd = leaf(L,_),
    _leeftijd = leaf(L,_),
    persoonlijke_pensioengerechtigde_leeftijd(Geboortedatum, _leeftijd,
        calculation(
            expr("Geboortedatum"), txt("heeft de pensioengerechtigde leeftijd van"), expr("Leeftijd"),
            reason("nodig om de pensioengerechtigdheid te bepalen")),
        RefsTree).
