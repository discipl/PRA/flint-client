voor_1_januari_2013(Peildatum)
    :-
        datum_voor_datum(Peildatum, datum(2013,1,1))
    .

leeftijd_van_65_jaar(Leeftijd)
    :-
        Leeftijd = leeftijd(65,0)
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_voor_1_januari_2013(Leeftijd, Peildatum)
    :-
        voor_1_januari_2013(Peildatum),
        leeftijd_van_65_jaar(Leeftijd)
    .

in_2013(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2013))
    .

leeftijd_van_65_jaar_en_1_maand(Leeftijd)
    :-
        Leeftijd = leeftijd(65,1)
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_en_1_maand_in_2013(Leeftijd, Peildatum)
    :-
        in_2013(Peildatum),
        leeftijd_van_65_jaar_en_1_maand(Leeftijd)
    .

in_2014(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2014))
    .

leeftijd_van_65_jaar_en_2_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(65,2)
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_en_2_maanden_in_2014(Leeftijd, Peildatum)
    :-
        in_2014(Peildatum),
        leeftijd_van_65_jaar_en_2_maanden(Leeftijd)
    .

in_2015(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2015))
    .

leeftijd_van_65_jaar_en_3_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(65,3)
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_en_3_maanden_in_2015(Leeftijd, Peildatum)
    :-
        in_2015(Peildatum),
        leeftijd_van_65_jaar_en_3_maanden(Leeftijd)
    .

in_2016(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2016))
    .

leeftijd_van_65_jaar_en_6_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(65,6)
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_en_6_maanden_in_2016(Leeftijd, Peildatum)
    :-
        in_2016(Peildatum),
        leeftijd_van_65_jaar_en_6_maanden(Leeftijd)
    .

in_2017(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2017))
    .

leeftijd_van_65_jaar_en_9_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(65,9)
    .

de_pensioengerechtigde_leeftijd_van_65_jaar_en_9_maanden_in_2017(Leeftijd, Peildatum)
    :-
        in_2017(Peildatum),
        leeftijd_van_65_jaar_en_9_maanden(Leeftijd)
    .

in_2018(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2018))
    .

leeftijd_van_66_jaar(Leeftijd)
    :-
        Leeftijd = leeftijd(66,0)
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_in_2018(Leeftijd, Peildatum)
    :-
        in_2018(Peildatum),
        leeftijd_van_66_jaar(Leeftijd)
    .

in_2019(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2019))
    .

leeftijd_van_66_jaar_en_4_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(66,4)
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2019(Leeftijd, Peildatum)
    :-
        in_2019(Peildatum),
        leeftijd_van_66_jaar_en_4_maanden(Leeftijd)
    .

in_2020(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2020))
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2020(Leeftijd, Peildatum)
    :-
        in_2020(Peildatum),
        leeftijd_van_66_jaar_en_4_maanden(Leeftijd)
    .

in_2021(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2021))
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2021(Leeftijd, Peildatum)
    :-
        in_2021(Peildatum),
        leeftijd_van_66_jaar_en_4_maanden(Leeftijd)
    .

in_2022(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2022))
    .

leeftijd_van_66_jaar_en_7_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(66,7)
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_en_7_maanden_in_2022(Leeftijd, Peildatum)
    :-
        in_2022(Peildatum),
        leeftijd_van_66_jaar_en_7_maanden(Leeftijd)
    .

in_2023(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2023))
    .

leeftijd_van_66_jaar_en_10_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(66,10)
    .

de_pensioengerechtigde_leeftijd_van_66_jaar_en_10_maanden_in_2023(Leeftijd, Peildatum)
    :-
        in_2023(Peildatum),
        leeftijd_van_66_jaar_en_10_maanden(Leeftijd)
    .

in_2024(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2024))
    .

leeftijd_van_67_jaar(Leeftijd)
    :-
        Leeftijd = leeftijd(67,0)
    .

de_pensioengerechtigde_leeftijd_van_67_jaar_in_2024(Leeftijd, Peildatum)
    :-
        in_2024(Peildatum),
        leeftijd_van_67_jaar(Leeftijd)
    .

in_2025(Peildatum)
    :-
        datum_in_jaar(Peildatum, jaar(2025))
    .

de_pensioengerechtigde_leeftijd_van_67_jaar_in_2025(Leeftijd, Peildatum)
    :-
        in_2025(Peildatum),
        leeftijd_van_67_jaar(Leeftijd)
    .

de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum)
    :-
        (   de_pensioengerechtigde_leeftijd_van_65_jaar_voor_1_januari_2013(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_65_jaar_en_1_maand_in_2013(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_65_jaar_en_2_maanden_in_2014(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_65_jaar_en_3_maanden_in_2015(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_65_jaar_en_6_maanden_in_2016(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_65_jaar_en_9_maanden_in_2017(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_66_jaar_in_2018(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2019(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2020(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_66_jaar_en_4_maanden_in_2021(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_66_jaar_en_7_maanden_in_2022(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_66_jaar_en_10_maanden_in_2023(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_67_jaar_in_2024(Leeftijd, Peildatum)
        ;   de_pensioengerechtigde_leeftijd_van_67_jaar_in_2025(Leeftijd, Peildatum)
        )
    .

bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, Peildatum)
    :-
        % huidige leeftijdsvoorwaarde
        de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum),
        % huidige leeftijdsvoorwaarde bereikt
        heeft_leeftijd_op_datum(bereikt, Geboortedatum, Leeftijd, Peildatum),
        % dag ervoor
        dag_voor_datum(_datumDagVoorPeildatum, Peildatum),
        % leeftijdsvoorwaarde van dag ervoor
        de_pensioengerechtigde_leeftijd(_leeftijdsvoorwaardeDagVoorPeildatum, _datumDagVoorPeildatum),
        % leeftijdsvoorwaarde nog niet bereikt dag ervoor
        heeft_leeftijd_op_datum(nog_niet_bereikt, Geboortedatum, _leeftijdsvoorwaardeDagVoorPeildatum, _datumDagVoorPeildatum),
        % leeftijd wordt voor het eerst bereikt
        \+ (datum_voor_datum(_nogEerderePeildatum, _datumDagVoorPeildatum),
            de_pensioengerechtigde_leeftijd(_nogEerdereLeeftijdsvoorwaarde, _nogEerderePeildatum),
            heeft_leeftijd_op_datum(bereikt, Geboortedatum, _nogEerdereLeeftijdsvoorwaarde, _nogEerderePeildatum)
            )
    .

persoonlijke_pensioengerechtigde_leeftijd(Geboortedatum, Leeftijd)
    :-
        bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, Peildatum),
        de_pensioengerechtigde_leeftijd(Leeftijd, Peildatum)
    .

leeftijd_van_15_jaar(Leeftijd)
    :-
        Leeftijd = leeftijd(15,0)
    .

de_aanvangsleeftijd_van_15_jaar_voor_1_januari_2013(Leeftijd, Peildatum)
    :-
        voor_1_januari_2013(Peildatum),
        leeftijd_van_15_jaar(Leeftijd)
    .

leeftijd_van_15_jaar_en_1_maand(Leeftijd)
    :-
        Leeftijd = leeftijd(15,1)
    .

de_aanvangsleeftijd_van_15_jaar_en_1_maand_in_2013(Leeftijd, Peildatum)
    :-
        in_2013(Peildatum),
        leeftijd_van_15_jaar_en_1_maand(Leeftijd)
    .

leeftijd_van_15_jaar_en_2_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(15,2)
    .

de_aanvangsleeftijd_van_15_jaar_en_2_maanden_in_2014(Leeftijd, Peildatum)
    :-
        in_2014(Peildatum),
        leeftijd_van_15_jaar_en_2_maanden(Leeftijd)
    .

leeftijd_van_15_jaar_en_3_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(15,3)
    .

de_aanvangsleeftijd_van_15_jaar_en_3_maanden_in_2015(Leeftijd, Peildatum)
    :-
        in_2015(Peildatum),
        leeftijd_van_15_jaar_en_3_maanden(Leeftijd)
    .

leeftijd_van_15_jaar_en_6_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(15,6)
    .

de_aanvangsleeftijd_van_15_jaar_en_6_maanden_in_2016(Leeftijd, Peildatum)
    :-
        in_2016(Peildatum),
        leeftijd_van_15_jaar_en_6_maanden(Leeftijd)
    .

leeftijd_van_15_jaar_en_9_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(15,9)
    .

de_aanvangsleeftijd_van_15_jaar_en_9_maanden_in_2017(Leeftijd, Peildatum)
    :-
        in_2017(Peildatum),
        leeftijd_van_15_jaar_en_9_maanden(Leeftijd)
    .

leeftijd_van_16_jaar(Leeftijd)
    :-
        Leeftijd = leeftijd(16,0)
    .

de_aanvangsleeftijd_van_16_jaar_in_2018(Leeftijd, Peildatum)
    :-
        in_2018(Peildatum),
        leeftijd_van_16_jaar(Leeftijd)
    .

leeftijd_van_16_jaar_en_4_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(16,4)
    .

de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2019(Leeftijd, Peildatum)
    :-
        in_2019(Peildatum),
        leeftijd_van_16_jaar_en_4_maanden(Leeftijd)
    .

de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2020(Leeftijd, Peildatum)
    :-
        in_2020(Peildatum),
        leeftijd_van_16_jaar_en_4_maanden(Leeftijd)
    .

de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2021(Leeftijd, Peildatum)
    :-
        in_2021(Peildatum),
        leeftijd_van_16_jaar_en_4_maanden(Leeftijd)
    .

leeftijd_van_16_jaar_en_7_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(16,7)
    .

de_aanvangsleeftijd_van_16_jaar_en_7_maanden_in_2022(Leeftijd, Peildatum)
    :-
        in_2022(Peildatum),
        leeftijd_van_16_jaar_en_7_maanden(Leeftijd)
    .

leeftijd_van_16_jaar_en_10_maanden(Leeftijd)
    :-
        Leeftijd = leeftijd(16,10)
    .

de_aanvangsleeftijd_van_16_jaar_en_10_maanden_in_2023(Leeftijd, Peildatum)
    :-
        in_2023(Peildatum),
        leeftijd_van_16_jaar_en_10_maanden(Leeftijd)
    .

leeftijd_van_17_jaar(Leeftijd)
    :-
        Leeftijd = leeftijd(17,0)
    .

de_aanvangsleeftijd_van_17_jaar_in_2024(Leeftijd, Peildatum)
    :-
        in_2024(Peildatum),
        leeftijd_van_17_jaar(Leeftijd)
    .

de_aanvangsleeftijd_van_17_jaar_in_2025(Leeftijd, Peildatum)
    :-
        in_2025(Peildatum),
        leeftijd_van_17_jaar(Leeftijd)
    .

de_aanvangsleeftijd(Leeftijd, Peildatum)
    :-
        (   de_aanvangsleeftijd_van_15_jaar_voor_1_januari_2013(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_15_jaar_en_1_maand_in_2013(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_15_jaar_en_2_maanden_in_2014(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_15_jaar_en_3_maanden_in_2015(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_15_jaar_en_6_maanden_in_2016(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_15_jaar_en_9_maanden_in_2017(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_16_jaar_in_2018(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2019(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2020(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_16_jaar_en_4_maanden_in_2021(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_16_jaar_en_7_maanden_in_2022(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_16_jaar_en_10_maanden_in_2023(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_17_jaar_in_2024(Leeftijd, Peildatum)
        ;   de_aanvangsleeftijd_van_17_jaar_in_2025(Leeftijd, Peildatum)
        )
    .

persoonlijke_aanvangsleeftijd(Geboortedatum, Leeftijd)
    :-
        bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, Peildatum),
        de_aanvangsleeftijd(Leeftijd, Peildatum)
    .

