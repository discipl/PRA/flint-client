
% gedeelte: het gedeelte van het gehele bedrag, in dit geval alles tot EuroTot,
% (gehele) bedrag: het gehele berag waar nog geen gedeelde van is genomen
% gedeeltelijk_berdag: het bedrag dat overblijft na het nemen van het gedeelte van het gehele bedrag
gedeelte_van_bedrag_als_gedeeltelijk_bedrag( tot(gehele_euros(EuroTot)),
                                             gehele_euros(TotaalBedrag),
                                             gehele_euros(GedeeltelijkBedrag) )
    :-
        finite(gehele_euros(EuroTot)),
        finite(gehele_euros(TotaalBedrag)),
        finite(gehele_euros(GedeeltelijkBedrag)),
        GedeeltelijkBedrag #= min(EuroTot, TotaalBedrag)
    .
% gedeelte: het gedeelte van het gehele bedrag, in dit geval alles boven EuroVanaf
% (gehele) bedrag: het gehele berag waar nog geen gedeelde van is genomen
% gedeeltelijk_berdag: het bedrag dat overblijft na het nemen van het gedeelte van het gehele bedrag
gedeelte_van_bedrag_als_gedeeltelijk_bedrag( vanaf(gehele_euros(EuroVanaf)),
                                             gehele_euros(TotaalBedrag),
                                             gehele_euros(GedeeltelijkBedrag) )
    :-
        finite(gehele_euros(EuroVanaf)),
        finite(gehele_euros(TotaalBedrag)),
        finite(gehele_euros(GedeeltelijkBedrag)),
        GedeeltelijkBedrag #= max(0, TotaalBedrag - EuroVanaf)
    .

% gedeelte: het gedeelte van het gehele bedrag, in dit geval een vantot range
% (gehele) bedrag: het gehele berag waar nog geen gedeelde van is genomen
% gedeeltelijk_berdag: het bedrag dat overblijft na het nemen van het gedeelte van het gehele bedrag
gedeelte_van_bedrag_als_gedeeltelijk_bedrag( vantot(gehele_euros(EuroVan), gehele_euros(EuroTot)),
                                             gehele_euros(TotaalBedrag),
                                             gehele_euros(GedeeltelijkBedrag) )
    :-
        finite(gehele_euros(EuroTot)),
        finite(gehele_euros(EuroTot)),
        finite(gehele_euros(TotaalBedrag)),
        finite(gehele_euros(GedeeltelijkBedrag)),
        finite(gehele_euros(_totBedrag)),
        _totBedrag #= min(TotaalBedrag, EuroTot) #/\ % GedeeltelijkBedrag bevat alleen het gedeelte van TotaalBedrag tot EuroTot, of minder
        GedeeltelijkBedrag #= max(0, _totBedrag - EuroVan) % het bedrag voor EuroVan wordt niet meegenomen, en
                                                           % GedeeltelijkBedrag kan niet negatief zijn
    .

gedeelte_als_percentage_van_bedrag( gehele_euros(Belasting),
                                    fraction(Amount, DevidedByTenToThePowerOf),
                                    gehele_euros(Bedrag) )
    :-
        Belasting #= (Bedrag * Amount) // (10^DevidedByTenToThePowerOf)
    .
gedeelte_als_percentage_van_bedrag( Belasting, Percentage, Bedrag )
    :-
        percentage_as_fraction(Percentage, Fraction),
        gedeelte_als_percentage_van_bedrag(Belasting, Fraction, Bedrag)
    .

bedrag_verminderd_met_doch_niet_verder_dan_nihil(gehele_euros(Bedrag), gehele_euros(Verminderd), gehele_euros(Met))
    :-
        Verminderd #= max(Bedrag-Met, 0)
    .

valt_binnen(gehele_euros(Bedrag), tot(gehele_euros(Tot))) :- Bedrag #< Tot .
valt_binnen(gehele_euros(Bedrag), vantot(gehele_euros(Van), gehele_euros(Tot))) :- Van #=< Bedrag #/\ Bedrag #< Tot .
valt_binnen(gehele_euros(Bedrag), vanaf(gehele_euros(Van))) :- Van #=< Bedrag .

%================================================================================%
%========= tot hier wordt gebruikt door plain_data en traceable_data ============%
%================================================================================%

inkomstenbelasting(BelastbaarInkomen, Belasting, Geboortedatum, Peildatum)
    :-
        bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, PglDatum),
        inkomstenbelasting(BelastbaarInkomen, Belasting, Geboortedatum, pgl(PglDatum), Peildatum)
    .

inkomstenbelasting(BelastbaarInkomen, Belasting, Geboortedatum, pgl(PglDatum), Peildatum) :-
        geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(Geboortedatum, pgl(PglDatum),
                                                                    Gedeelte,
                                                                    MinimaaleBelasting,
                                                                    Percentage,
                                                                    Peildatum),
        valt_binnen(BelastbaarInkomen, Gedeelte),
        gedeelte_van_bedrag_als_gedeeltelijk_bedrag(Gedeelte, BelastbaarInkomen, BelastbaarInkomenGedeelte),
        gedeelte_als_percentage_van_bedrag(VermeerderBelasting, Percentage, BelastbaarInkomenGedeelte),
        bedrag_bedrag_sum(MinimaaleBelasting, VermeerderBelasting, Belasting)
   .

loonheffing2(
            %aow(AlleenstaandeouderenkortingToekenning),
            aow(_),
            Geboortedatum, Peildatum,
            BrutoInkomen, NettoLoonheffing, NettoInkomen
        )
    :-
        bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, PglDatum),
        %alleenstaandeouderenkorting(Alleenstaandeouderenkorting, AlleenstaandeouderenkortingToekenning, Peildatum),
        ouderenkorting_bij_verzamelinkomen(Ouderenkorting, BrutoInkomen, PglDatum, Peildatum),
        %totaal_van(Loonheffingskorting, [Ouderenkorting, Alleenstaandeouderenkorting]),
        Loonheffingskorting = Ouderenkorting,
        inkomstenbelasting(BrutoInkomen, BrutoLoonheffing, Geboortedatum, pgl(PglDatum), Peildatum),
        bedrag_bedrag_sum(Loonheffingskorting, NettoLoonheffing, BrutoLoonheffing),
        bedrag_bedrag_sum(NettoLoonheffing, NettoInkomen, BrutoInkomen)
    .

loonheffing2(geen_aow, Geboortedatum, Peildatum, BrutoInkomen, NettoLoonheffing, NettoInkomen)
    :-
        inkomstenbelasting(BrutoInkomen, BrutoLoonheffing, Geboortedatum, Peildatum),
        BrutoLoonheffing = NettoLoonheffing,
        bedrag_bedrag_sum(NettoLoonheffing, NettoInkomen, BrutoInkomen)
    .


%================================================================================%
%========= tot hier wordt gebruikt door raceable_data ============%
%================================================================================%

voor_belastbaar_inkomen_de_belasting_als_percentage_over_gedeelte( BelastbaarInkomen,
                                                                   Belasting,
                                                                   Percentage,
                                                                   Gedeelte )
    :-
        %Belasting = gehele_euros(BelastingHoeveelheid),
        %BelastbaarInkomen = gehele_euros(BelastbaarInkomenHoeveelheid),
        gedeelte_van_bedrag_als_gedeeltelijk_bedrag(Gedeelte, BelastbaarInkomen, BelastbaarInkomenGedeelte),
        gedeelte_als_percentage_van_bedrag(Belasting, Percentage, BelastbaarInkomenGedeelte)
    .

%voor_belastbaar_inkomen_de_belasting_als_percentage_over_gedeelte( gehele_euros(100000),
%                                                                   gehele_euros(Belasting),
%                                                                   percentage(19,17),
%                                                                   tot(gehele_euros(30000)) ).



geboortedatum_box_belastbaar_inkomen_belasting_kalenderjaar( Geboortedatum,
                                                             box(1),
                                                             BelastbaarInkomen,
                                                             Belasting,
                                                             Kalenderjaar )
    :-
        findall(
            [Gedeelte, Percentage],
            geboortedatum_box_schijf_belastbaar_inkomen_gedeelte_percentage_kalenderjaar(
                Geboortedatum,
                box(1),
                Gedeelte,
                Percentage,
                Kalenderjaar ),
            GedeeltePerecentageList ),
        transpose(GedeeltePerecentageList, [Gedeeltes, Percentages]),
        maplist(voor_belastbaar_inkomen_de_belasting_als_percentage_over_gedeelte(BelastbaarInkomen),
                BelastingPerSchijf, Percentages, Gedeeltes),
        totaal_van(Belasting, BelastingPerSchijf)
    .

% loonheffing berekening voor AOW gerechtigden
loonheffing(Geboortedatum, Kalenderjaar, aow(AlleenstaandeouderenkortingToekenning),
            BrutoInkomen, NettoLoonheffing, NettoInkomen
            %,[ouderenkorting(Ouderenkorting), alleenstaandeouderenkorting(Alleenstaandeouderenkorting)]
            )
    :-
        alleenstaandeouderenkorting_in_kalenderjaar(Alleenstaandeouderenkorting, AlleenstaandeouderenkortingToekenning, Kalenderjaar),
        ouderenkorting_bij_verzamelinkomen_in_kalenderjaar(Ouderenkorting, BrutoInkomen, Kalenderjaar),
        totaal_van(Loonheffingskorting, [Ouderenkorting, Alleenstaandeouderenkorting]),
        bedrag_bedrag_sum(Loonheffingskorting, NettoLoonheffing, BrutoLoonheffing),
        geboortedatum_box_belastbaar_inkomen_belasting_kalenderjaar( Geboortedatum,
                                                                     box(1),
                                                                     BrutoInkomen,
                                                                     BrutoLoonheffing,
                                                                     Kalenderjaar  ),
        bedrag_bedrag_sum(NettoLoonheffing, NettoInkomen, BrutoInkomen)
    .

% normale loonheffing berekening
loonheffing(Geboortedatum, Kalenderjaar, geen_aow,
            BrutoInkomen, Loonheffing, NettoInkomen
            %,[]
            )
    :-
        geboortedatum_box_belastbaar_inkomen_belasting_kalenderjaar( Geboortedatum,
                                                                     box(1),
                                                                     BrutoInkomen,
                                                                     Loonheffing,
                                                                     Kalenderjaar  ),
        bedrag_bedrag_sum(Loonheffing, NettoInkomen, BrutoInkomen)
    .

