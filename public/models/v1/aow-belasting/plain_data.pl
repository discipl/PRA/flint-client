
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022,  1, _), percentage( 9,32), gehele_euros(3341), gehele_euros(15917), datum(2022,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022,  2, _), percentage(10,81), gehele_euros(3869), gehele_euros(16445), datum(2022,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022,  3, _), percentage(12,30), gehele_euros(4398), gehele_euros(16974), datum(2022,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022,  4, _), percentage(13,79), gehele_euros(4927), gehele_euros(17503), datum(2022,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022,  5, _), percentage(15,29), gehele_euros(5455), gehele_euros(18031), datum(2022,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022,  6, _), percentage(16,78), gehele_euros(5984), gehele_euros(18560), datum(2022,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022,  7, _), percentage(18,27), gehele_euros(6516), gehele_euros(19092), datum(2022,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022,  8, _), percentage(19,76), gehele_euros(7044), gehele_euros(19620), datum(2022,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022,  9, _), percentage(21,25), gehele_euros(7573), gehele_euros(20149), datum(2022,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022, 10, _), percentage(22,75), gehele_euros(8101), gehele_euros(20677), datum(2022,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022, 11, _), percentage(24,24), gehele_euros(8630), gehele_euros(21206), datum(2022,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2022, 12, _), percentage(25,73), gehele_euros(9158), gehele_euros(21734), datum(2022,_,_)).

aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023,  1, _), percentage( 9,28), gehele_euros(3447), gehele_euros(16698), datum(2023,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023,  2, _), percentage(10,77), gehele_euros(4000), gehele_euros(17251), datum(2023,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023,  3, _), percentage(12,26), gehele_euros(4554), gehele_euros(17805), datum(2023,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023,  4, _), percentage(13,76), gehele_euros(5111), gehele_euros(18362), datum(2023,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023,  5, _), percentage(15,25), gehele_euros(5665), gehele_euros(18916), datum(2023,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023,  6, _), percentage(16,74), gehele_euros(6218), gehele_euros(19469), datum(2023,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023,  7, _), percentage(18,23), gehele_euros(6772), gehele_euros(20023), datum(2023,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023,  8, _), percentage(19,72), gehele_euros(7325), gehele_euros(20576), datum(2023,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023,  9, _), percentage(21,21), gehele_euros(7879), gehele_euros(21130), datum(2023,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023, 10, _), percentage(22,71), gehele_euros(8436), gehele_euros(21687), datum(2023,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023, 11, _), percentage(24,20), gehele_euros(8990), gehele_euros(22241), datum(2023,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2023, 12, _), percentage(25,69), gehele_euros(9543), gehele_euros(22794), datum(2023,_,_)).

aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024,  1, _), percentage( 9,42), gehele_euros(3550), gehele_euros(17384), datum(2024,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024,  2, _), percentage(10,91), gehele_euros(4118), gehele_euros(17952), datum(2024,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024,  3, _), percentage(12,40), gehele_euros(4686), gehele_euros(18520), datum(2024,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024,  4, _), percentage(13,89), gehele_euros(5253), gehele_euros(19087), datum(2024,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024,  5, _), percentage(15,38), gehele_euros(5825), gehele_euros(19659), datum(2024,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024,  6, _), percentage(16,87), gehele_euros(6392), gehele_euros(20226), datum(2024,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024,  7, _), percentage(18,37), gehele_euros(6960), gehele_euros(20794), datum(2024,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024,  8, _), percentage(19,86), gehele_euros(7528), gehele_euros(21362), datum(2024,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024,  9, _), percentage(21,35), gehele_euros(8095), gehele_euros(21929), datum(2024,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024, 10, _), percentage(22,84), gehele_euros(8667), gehele_euros(22501), datum(2024,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024, 11, _), percentage(24,33), gehele_euros(9234), gehele_euros(23068), datum(2024,_,_)).
aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(datum(2024, 12, _), percentage(25,82), gehele_euros(9802), gehele_euros(23636), datum(2024,_,_)).

geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum,
                                Grens,
                                MinimaaleBelasting,
                                Percentage,
                                Peildatum) :-
    bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum,
                                            PglDatum),
    geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                    Geboortedatum, pgl(PglDatum),
                                    Grens,
                                    MinimaaleBelasting,
                                    Percentage,
                                    Peildatum).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Voor mensen geboren voor 1 januari 1946, 1e schijf voor 2022
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                tot(gehele_euros(36409)),
                                gehele_euros(0),
                                percentage(9,42),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_voor_datum(Geboortedatum,
                     datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2022,1,1)).

% Voor mensen geboren voor 1 januari 1946, 2e schijf voor 2022
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vantot(gehele_euros(36409), gehele_euros(69398)),
                                gehele_euros(3429),
                                percentage(37,07),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_voor_datum(Geboortedatum,
                     datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2022,1,1)).

% Voor mensen geboren voor 1 januari 1946, 3e schijf voor 2022
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vanaf(gehele_euros(69398)),
                                gehele_euros(15917),
                                percentage(49,50),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_voor_datum(Geboortedatum,
                     datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2022,1,1)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Voor mensen geboren voor 1 januari 1946, 1e schijf voor 2023
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                tot(gehele_euros(38703)),
                                gehele_euros(0),
                                percentage(9,28),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_voor_datum(Geboortedatum,
                     datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2023,1,1)).

% Voor mensen geboren voor 1 januari 1946, 2e schijf voor 2023
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vantot(gehele_euros(38703), gehele_euros(73031)),
                                gehele_euros(3591),
                                percentage(36,93),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_voor_datum(Geboortedatum,
                     datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2023,1,1)).

% Voor mensen geboren voor 1 januari 1946, 3e schijf voor 2023
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vanaf(gehele_euros(73031)),
                                gehele_euros(16268),
                                percentage(49,50),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_voor_datum(Geboortedatum,
                     datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2023,1,1)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Voor mensen geboren voor 1 januari 1946, 1e schijf voor 2024
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                tot(gehele_euros(40021)),
                                gehele_euros(0),
                                percentage(9,32),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_voor_datum(Geboortedatum,
                     datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2024,1,1)).

% Voor mensen geboren voor 1 januari 1946, 2e schijf voor 2024
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vantot(gehele_euros(40021), gehele_euros(75518)),
                                gehele_euros(3729),
                                percentage(36,97),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_voor_datum(Geboortedatum,
                     datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2024,1,1)).

% Voor mensen geboren voor 1 januari 1946, 3e schijf voor 2024
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vanaf(gehele_euros(75518)),
                                gehele_euros(16852),
                                percentage(49,50),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_voor_datum(Geboortedatum,
                     datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2024,1,1)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Voor mensen geboren op of na 1 januari 1946, 1e schijf voor 2022
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                tot(gehele_euros(35472)),
                                gehele_euros(0),
                                percentage(9,42),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_op_of_na_datum(Geboortedatum,
                         datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2022,1,1)).

% Voor mensen geboren op of na 1 januari 1946, 2e schijf voor 2022
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vantot(gehele_euros(35472), gehele_euros(69398)),
                                gehele_euros(3341),
                                percentage(37,07),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_op_of_na_datum(Geboortedatum,
                         datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2022,1,1)).

% Voor mensen geboren op of na 1 januari 1946, 3e schijf voor 2022
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vanaf(gehele_euros(69398)),
                                gehele_euros(15917),
                                percentage(49,50),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_op_of_na_datum(Geboortedatum,
                         datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2022,1,1)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Voor mensen geboren op of na 1 januari 1946, 1e schijf voor 2023
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                tot(gehele_euros(37149)),
                                gehele_euros(0),
                                percentage(9,28),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_op_of_na_datum(Geboortedatum,
                         datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2023,1,1)).

% Voor mensen geboren op of na 1 januari 1946, 2e schijf voor 2023
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vantot(gehele_euros(37149), gehele_euros(73031)),
                                gehele_euros(3447),
                                percentage(36,93),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_op_of_na_datum(Geboortedatum,
                         datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2023,1,1)).

% Voor mensen geboren op of na 1 januari 1946, 3e schijf voor 2023
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vanaf(gehele_euros(73031)),
                                gehele_euros(16698),
                                percentage(49,50),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_op_of_na_datum(Geboortedatum,
                         datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikt voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2023,1,1)).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Voor mensen geboren op of na 1 januari 1946, 1e schijf voor 2024
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                tot(gehele_euros(38098)),
                                gehele_euros(0),
                                percentage(9,32),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_op_of_na_datum(Geboortedatum,
                         datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikd voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2024,1,1)).

% Voor mensen geboren op of na 1 januari 1946, 2e schijf voor 2024
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vantot(gehele_euros(38098), gehele_euros(75518)),
                                gehele_euros(3550),
                                percentage(36,97),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_op_of_na_datum(Geboortedatum,
                         datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikd voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2024,1,1)).


% Voor mensen geboren op of na 1 januari 1946, 3e schijf voor 2024
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                Geboortedatum, pgl(PglDatum),
                                vanaf(gehele_euros(75518)),
                                gehele_euros(17384),
                                percentage(49,50),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_op_of_na_datum(Geboortedatum,
                         datum(1946,1,1)),
    % hebben al de pensioengerechtigde leeftijd bereikd voor aanvang jaar
    datum_voor_datum(PglDatum,
                     datum(2024,1,1)).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Voor mensen die de aow leeftijd bereiken in 2022 1e schijf voor 2022
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                _, pgl(PglDatum),
                                tot(gehele_euros(35473)),
                                gehele_euros(0),
                                Percentage,
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_in_jaar(PglDatum, jaar(2022)),
    aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(PglDatum, Percentage, _, _, Peildatum).

% Voor mensen die de aow leeftijd bereiken in 2022 2e schijf voor 2022
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                _, pgl(PglDatum),
                                vantot(gehele_euros(35473), gehele_euros(69399)),
                                MinimaalBedrag2,
                                percentage(37,07),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_in_jaar(PglDatum, jaar(2022)),
    aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(PglDatum, _, MinimaalBedrag2, _, Peildatum).

% Voor mensen die de aow leeftijd bereiken in 2022 3e schijf voor 2022
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                _, pgl(PglDatum),
                                vanaf(gehele_euros(69399)),
                                MinimaalBedrag3,
                                percentage(49,50),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_in_jaar(PglDatum, jaar(2022)),
    aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(PglDatum, _, _, MinimaalBedrag3, Peildatum).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Voor mensen die de aow leeftijd bereiken in 2023 1e schijf voor 2023
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                _, pgl(PglDatum),
                                tot(gehele_euros(37149)),
                                gehele_euros(0),
                                Percentage,
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_in_jaar(PglDatum, jaar(2023)),
    aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(PglDatum, Percentage, _, _, Peildatum).

% Voor mensen die de aow leeftijd bereiken in 2023 2e schijf voor 2023
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                _, pgl(PglDatum),
                                vantot(gehele_euros(37149), gehele_euros(73031)),
                                MinimaalBedrag2,
                                percentage(36,93),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_in_jaar(PglDatum, jaar(2023)),
    aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(PglDatum, _, MinimaalBedrag2, _, Peildatum).

% Voor mensen die de aow leeftijd bereiken in 2023 3e schijf voor 2023
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                _, pgl(PglDatum),
                                vanaf(gehele_euros(73031)),
                                MinimaalBedrag3,
                                percentage(49,50),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_in_jaar(PglDatum, jaar(2023)),
    aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(PglDatum, _, _, MinimaalBedrag3, Peildatum).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Voor mensen die de aow leeftijd bereiken in 2024 1e schijf voor 2024
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                _, pgl(PglDatum),
                                tot(gehele_euros(38098)),
                                gehele_euros(0),
                                Percentage,
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_in_jaar(PglDatum, jaar(2024)),
    aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(PglDatum, Percentage, _, _, Peildatum).

% Voor mensen die de aow leeftijd bereiken in 2024 2e schijf voor 2024
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                _, pgl(PglDatum),
                                vantot(gehele_euros(38098), gehele_euros(75518)),
                                MinimaalBedrag2,
                                percentage(36,97),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_in_jaar(PglDatum, jaar(2024)),
    aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(PglDatum, _, MinimaalBedrag2, _, Peildatum).


% Voor mensen die de aow leeftijd bereiken in 2024 3e schijf voor 2024
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(
                                _, pgl(PglDatum),
                                vanaf(gehele_euros(75518)),
                                MinimaalBedrag3,
                                percentage(49,50),
                                Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_in_jaar(PglDatum, jaar(2024)),
    aow_leeftijd_bereikt_op_datum__percentage1__minimaalbedrag2__minimaalbedrag3(PglDatum, _, _, MinimaalBedrag3, Peildatum).


% er zijn maar 2 schijven voor wanneer de AOW-leeftijd nog niet bereikt is aan het einde van
% het jaar

% AOW-leeftijd nog niet bereikt aan het eind van in 2022, 1e schijf
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(_, pgl(PglDatum),
                                                            tot(gehele_euros(69399)),
                                                            gehele_euros(0),
                                                            percentage(37,07),
                                                            Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_voor_datum(datum(2022,12,31), PglDatum).

% AOW-leeftijd nog niet bereikt aan het eind van in 2022, 2e schijf
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(_, pgl(PglDatum),
                                                            vanaf(gehele_euros(69399)),
                                                            gehele_euros(15917),
                                                            percentage(49,50),
                                                            Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_voor_datum(datum(2022,12,31), PglDatum).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% AOW-leeftijd nog niet bereikt aan het eind van in 2023, 1e schijf
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(_, pgl(PglDatum),
                                                            tot(gehele_euros(73031)),
                                                            gehele_euros(0),
                                                            percentage(36,93),
                                                            Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_voor_datum(datum(2023,12,31), PglDatum).

% AOW-leeftijd nog niet bereikt aan het eind van in 2023, 2e schijf
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(_, pgl(PglDatum),
                                                            vanaf(gehele_euros(73031)),
                                                            gehele_euros(16698),
                                                            percentage(49,50),
                                                            Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_voor_datum(datum(2023,12,31), PglDatum).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% AOW-leeftijd nog niet bereikt aan het eind van in 2024, 1e schijf
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(_, pgl(PglDatum),
                                                            tot(gehele_euros(75518)),
                                                            gehele_euros(0),
                                                            percentage(36,97),
                                                            Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_voor_datum(datum(2024,12,31), PglDatum).

% AOW-leeftijd nog niet bereikt aan het eind van in 2024, 2e schijf
geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(_, pgl(PglDatum),
                                                            vanaf(gehele_euros(75518)),
                                                            gehele_euros(17384),
                                                            percentage(49,50),
                                                            Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_voor_datum(datum(2024,12,31), PglDatum).


% De ouderenkorting is een korting op de belasting, deze is afhankelijke van het verzamelinkomen en het huidige jaar
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ouderenkorting niet van toepassing 2022
ouderenkorting_bij_verzamelinkomen(gehele_euros(0),
                                   _,
                                   PglDatum, Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_na_datum(PglDatum,
                   datum(2022,12,31)).


% Ouderenkorting berekening in 2022 verzamelinkomen onder de grens
ouderenkorting_bij_verzamelinkomen(gehele_euros(1726),
                                   Verzamelinkomen,
                                   PglDatum, Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_op_of_voor_datum(PglDatum,
                           datum(2022,12,31)),
    valt_binnen(Verzamelinkomen, tot(gehele_euros(38464))).

% Ouderenkorting berekening in 2022 verzamelinkomen boven de grens
ouderenkorting_bij_verzamelinkomen(Ouderenkorting,
                                   Verzamelinkomen,
                                   PglDatum, Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2022)),
    datum_op_of_voor_datum(PglDatum,
                           datum(2022,12,31)),
    Grens = vanaf(gehele_euros(38464)),
    valt_binnen(Verzamelinkomen, Grens),
    % boven de grens
    gedeelte_van_bedrag_als_gedeeltelijk_bedrag(Grens, Verzamelinkomen, VerzamelinkomenBovenDeGrens),
    % procent van boven de grens
    gedeelte_als_percentage_van_bedrag(XProcentVanHetVerzamelinkomenBovenDeGrens,
                                       percentage(15,0),
                                       VerzamelinkomenBovenDeGrens),
    % ouderenkorting verminderd met procent van boven de grens
    bedrag_verminderd_met_doch_niet_verder_dan_nihil(gehele_euros(1726),
                                                     Ouderenkorting,
                                                     XProcentVanHetVerzamelinkomenBovenDeGrens).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ouderenkorting niet van toepassing 2023
ouderenkorting_bij_verzamelinkomen(gehele_euros(0),
                                   _,
                                   PglDatum, Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_na_datum(PglDatum,
                   datum(2023,12,31)).

% Ouderenkorting berekening in 2023 verzamelinkomen onder de grens
ouderenkorting_bij_verzamelinkomen(gehele_euros(1835),
                                   Verzamelinkomen,
                                   PglDatum, Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_op_of_voor_datum(PglDatum,
                           datum(2023,12,31)),
    valt_binnen(Verzamelinkomen, tot(gehele_euros(40888))).

% Ouderenkorting berekening in 2023 verzamelinkomen boven de grens
ouderenkorting_bij_verzamelinkomen(Ouderenkorting,
                                   Verzamelinkomen,
                                   PglDatum, Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2023)),
    datum_op_of_voor_datum(PglDatum,
                           datum(2023,12,31)),
    Grens = vanaf(gehele_euros(40888)),
    valt_binnen(Verzamelinkomen, Grens),
    % boven de grens
    gedeelte_van_bedrag_als_gedeeltelijk_bedrag(Grens, Verzamelinkomen, VerzamelinkomenBovenDeGrens),
    % procent van boven de grens
    gedeelte_als_percentage_van_bedrag(XProcentVanHetVerzamelinkomenBovenDeGrens,
                                       percentage(15,0),
                                       VerzamelinkomenBovenDeGrens),
    % ouderenkorting verminderd met procent van boven de grens
    bedrag_verminderd_met_doch_niet_verder_dan_nihil(gehele_euros(1835),
                                                     Ouderenkorting,
                                                     XProcentVanHetVerzamelinkomenBovenDeGrens).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ouderenkorting niet van toepassing 2024
ouderenkorting_bij_verzamelinkomen(gehele_euros(0),
                                   _,
                                   PglDatum, Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_na_datum(PglDatum,
                   datum(2024,12,31)).

% Ouderenkorting berekening in 2024 verzamelinkomen onder de grens
ouderenkorting_bij_verzamelinkomen(gehele_euros(2010),
                                   Verzamelinkomen,
                                   PglDatum, Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_op_of_voor_datum(PglDatum,
                           datum(2024,12,31)),
    valt_binnen(Verzamelinkomen, tot(gehele_euros(44770))).

% Ouderenkorting berekening in 2024 verzamelinkomen boven de grens
ouderenkorting_bij_verzamelinkomen(Ouderenkorting,
                                   Verzamelinkomen,
                                   PglDatum, Peildatum) :-
    datum_in_jaar(Peildatum, jaar(2024)),
    datum_op_of_voor_datum(PglDatum,
                           datum(2024,12,31)),
    Grens = vanaf(gehele_euros(44770)),
    valt_binnen(Verzamelinkomen, Grens),
    % boven de grens
    gedeelte_van_bedrag_als_gedeeltelijk_bedrag(Grens, Verzamelinkomen, VerzamelinkomenBovenDeGrens),
    % procent van boven de grens
    gedeelte_als_percentage_van_bedrag(XProcentVanHetVerzamelinkomenBovenDeGrens,
                                       percentage(15,0),
                                       VerzamelinkomenBovenDeGrens),
    % ouderenkorting verminderd met procent van boven de grens
    bedrag_verminderd_met_doch_niet_verder_dan_nihil(gehele_euros(2010),
                                                     Ouderenkorting,
                                                     XProcentVanHetVerzamelinkomenBovenDeGrens).

% De alleenstaandeouderenkorting is een korting op de belasting, dit is elk jaar een vast bedrag
% het kan ook toegekend worden in andere gevallen dan alleen aan alleenstaande ouderen.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%alleenstaandeouderenkorting_in_kalenderjaar(gehele_euros(0), samenwonend, _).
%alleenstaandeouderenkorting_in_kalenderjaar(gehele_euros(449), alleenstaand, jaar(2022)).
%alleenstaandeouderenkorting_in_kalenderjaar(gehele_euros(478), alleenstaand, jaar(2023)).
%alleenstaandeouderenkorting_in_kalenderjaar(gehele_euros(524), alleenstaand, jaar(2024)).
