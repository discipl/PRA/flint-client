% -*- Prolog -*-

gedeelte_van_bedrag_als_gedeeltelijk_bedrag(
            leaf(Gedeelte, SubRef1),
            leaf(Bedrag, SubRef2),
            leaf(GedeeltelijkBedrag, SubRef3),
            Ref, node(
				base(Ref),
                operator("gedeelte_van_bedrag_als_gedeeltelijk_bedrag", ref([])),
                children([
                    leaf(Gedeelte, SubRef1),
                    leaf(Bedrag, SubRef2),
                    leaf(GedeeltelijkBedrag, SubRef3)
                ])))
    :- gedeelte_van_bedrag_als_gedeeltelijk_bedrag(Gedeelte, Bedrag, GedeeltelijkBedrag) .

gedeelte_als_percentage_van_bedrag(
            leaf(Gedeelte, SubRef1),
            leaf(Percentage, SubRef2),
            leaf(Bedrag, SubRef3),
            Ref, node(
				base(Ref),
                operator("gedeelte_als_percentage_van_bedrag", ref([])),
                children([
                    leaf(Gedeelte, SubRef1),
                    leaf(Percentage, SubRef2),
                    leaf(Bedrag, SubRef3)
                ])))
    :- gedeelte_als_percentage_van_bedrag(Gedeelte, Percentage, Bedrag) .

bedrag_verminderd_met_doch_niet_verder_dan_nihil(
            leaf(Bedrag, SubRef1),
            leaf(Verminderd, SubRef2),
            leaf(Met, SubRef3),
            Ref, node(
				base(Ref),
                operator("bedrag_verminderd_met_doch_niet_verder_dan_nihil", ref([])),
                children([
                    leaf(Bedrag, SubRef1),
                    leaf(Verminderd, SubRef2),
                    leaf(Met, SubRef3)
                ])))
    :- bedrag_verminderd_met_doch_niet_verder_dan_nihil(Bedrag, Verminderd, Met) .

valt_binnen(
            leaf(Bedrag, SubRef1),
            leaf(Bereik, SubRef2),
            Ref, node(
				base(Ref),
                operator("valt_binnen", ref([])),
                children([
                    leaf(Bedrag, SubRef1),
                    leaf(Bereik, SubRef2)
                ])))
    :- valt_binnen(Bedrag, Bereik) .

%================================================================================%
%========= tot hier wordt gebruikt door plain_data en traceable_data ============%
%================================================================================%

inkomstenbelasting(BelastbaarInkomen, Belasting, Geboortedatum, Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("inkomstenbelasting", UsageRef),
            operator("and", ref([])),
            children([Tree_0, Tree_1])
        ),
        bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, PglDatum,
                                                ref([]), Tree_0),
        inkomstenbelasting(BelastbaarInkomen, Belasting, Geboortedatum, pgl(PglDatum), Peildatum,
                           ref([]), Tree_1).

inkomstenbelasting(BelastbaarInkomen, Belasting, Geboortedatum, pgl(PglDatum), Peildatum, UsageRef, RefsTree)
    :-
        RefsTree = node(
            fact("inkomstenbelasting", UsageRef),
            operator("and", ref([])),
            children([Tree_1, Tree_2, Tree_3, Tree_4, Tree_5])
        ),
        geboortedatum_box1gedeelte_beginbedrag_percentage_peildatum(Geboortedatum, pgl(PglDatum),
                                                                    Gedeelte,
                                                                    MinimaaleBelasting,
                                                                    Percentage,
                                                                    Peildatum,
                                                                    ref([]), Tree_1),
        valt_binnen(BelastbaarInkomen, Gedeelte, ref([]), Tree_2),
        gedeelte_van_bedrag_als_gedeeltelijk_bedrag(Gedeelte, BelastbaarInkomen, BelastbaarInkomenGedeelte, ref([]), Tree_3),
        gedeelte_als_percentage_van_bedrag(VermeerderBelasting, Percentage, BelastbaarInkomenGedeelte, ref([]), Tree_4),
        bedrag_bedrag_sum(MinimaaleBelasting, VermeerderBelasting, Belasting, ref([]), Tree_5)
   .

loonheffing2(UsageRef,
             %aow(AlleenstaandeouderenkortingToekenning),
             aow(_),
             Geboortedatum, Peildatum,
             BrutoInkomen, NettoLoonheffing, NettoInkomen,
             RefsTree)
    :-
        RefsTree = node(
            fact("loonheffing aow", UsageRef),
            operator("and", ref([])),
            children([Tree_0, Tree_1, Tree_2, Tree_3, Tree_4])
        ),
        %alleenstaandeouderenkorting(Alleenstaandeouderenkorting, AlleenstaandeouderenkortingToekenning, Peildatum),
        bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, PglDatum,
                                                ref([]), Tree_0),
        ouderenkorting_bij_verzamelinkomen(Ouderenkorting, BrutoInkomen, PglDatum, Peildatum, ref([]), Tree_1),
        %totaal_van(Loonheffingskorting, [Ouderenkorting, Alleenstaandeouderenkorting]),
        Loonheffingskorting = Ouderenkorting,
        inkomstenbelasting(BrutoInkomen, BrutoLoonheffing, Geboortedatum, Peildatum, ref([]), Tree_2),
        bedrag_bedrag_sum(Loonheffingskorting, NettoLoonheffing, BrutoLoonheffing, ref([]), Tree_3),
        bedrag_bedrag_sum(NettoLoonheffing, NettoInkomen, BrutoInkomen, ref([]), Tree_4)
    .

loonheffing2(UsageRef,
             geen_aow,
             Geboortedatum, Peildatum,
             BrutoInkomen, NettoLoonheffing, NettoInkomen,
             RefsTree)
    :-
        RefsTree = node(
            fact("loonheffing", UsageRef),
            operator("and", ref([])),
            children([Tree_0, Tree_1, Tree_2])
        ),
        bereikt_de_pensioengerechtigde_leeftijd(Geboortedatum, PglDatum,
                                                ref([]), Tree_0),
        inkomstenbelasting(BrutoInkomen, BrutoLoonheffing, Geboortedatum, pgl(PglDatum), Peildatum, ref([]), Tree_1),
        BrutoLoonheffing = NettoLoonheffing,
        bedrag_bedrag_sum(NettoLoonheffing, NettoInkomen, BrutoInkomen, ref([]), Tree_2)
    .
