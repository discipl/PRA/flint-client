% -*- Prolog -*-
inkomstenbelasting_aow(
                    %inputs
                    Geboortedatum,
                    Kalenderjaar,
                    Alleenstaand,
                    AowBedrag,
                    PotjesBedragen,

                    %outputs
                    NettoPensioenAow,
                    NettoPensioenPerPotje,
                    LoonheffingAow,
                    LoonheffingPerPotje,
                    LoonheffingTotaal,

                    PotjesTotaal,
                    BrutoPensioenTotaal,
                    NettoPensioenTotaal,
                    Inkomstenbelasting,

                    NogTeBetalenInkomstenbelasting
                    )
    :-
        totaal_van(PotjesTotaal, PotjesBedragen),
        bedrag_bedrag_sum(AowBedrag, PotjesTotaal, BrutoPensioenTotaal),
        % Werkelijk in te houden bedrag
        datum_in_jaar(Peildatum, Kalenderjaar),
        loonheffing2(
            aow(Alleenstaand), Geboortedatum, Peildatum,
            BrutoPensioenTotaal, Inkomstenbelasting, NettoPensioenTotaal),
            % (NettoPensioenTotaal wordt verder niet gebruikt)
        % Alvast ingehouden door belastingdienst
        loonheffing2(
            aow(Alleenstaand), Geboortedatum, Peildatum,
            AowBedrag, LoonheffingAow, NettoPensioenAow),
        % Alvast ingehouden door de pensioenmaatschappijen
        maplist(loonheffing2(geen_aow, Geboortedatum, Peildatum),
                PotjesBedragen, LoonheffingPerPotje, NettoPensioenPerPotje),
        % (het NettoPensioenPerPotje en het NettoPensioenAow is niet nodig om te weten)
        totaal_van(LoonheffingTotaal, [LoonheffingAow|LoonheffingPerPotje]),
        bedrag_bedrag_sum(LoonheffingTotaal, NogTeBetalenInkomstenbelasting, Inkomstenbelasting)
    .

inkomstenbelasting_aow(
                    %inputs
                    Geboortedatum,
                    Kalenderjaar,
                    Alleenstaand,
                    AowBedrag,
                    PotjesBedragen,

                    %outputs
                    NettoPensioenAow,
                    NettoPensioenPerPotje,
                    LoonheffingAow,
                    LoonheffingPerPotje,
                    LoonheffingTotaal,

                    PotjesTotaal,
                    BrutoPensioenTotaal,
                    NettoPensioenTotaal,
                    Inkomstenbelasting,

                    NogTeBetalenInkomstenbelasting,
                    RefsTree
                    )
    :-
        RefsTree = node(
            fact(ref(["besluit_id2"]),
                calculation(
                    expr("NogTeBetalenInkomstenbelasting"), txt("terug te betalen van een netto inkomen van"), expr("NettoPensioenTotaal"),
                    reason("nodig voor de wordt_pensioengerechtigd_bij_leeftijd_op_datum functionaliteit"))),
            operator("and", ref([])),
            children([Tree_1, Tree_2, Tree_3, Tree_4, Tree_5, Tree_6, Tree_7 | MoreTrees])
        ),
        totaal_van(PotjesTotaal, PotjesBedragen, ref([]), Tree_1),
        bedrag_bedrag_sum(AowBedrag, PotjesTotaal, BrutoPensioenTotaal, ref([]), Tree_2),
        % Werkelijk in te houden bedrag
        datum_in_jaar(Peildatum, Kalenderjaar, ref([]), Tree_7),
        loonheffing2(ref([]),
            aow(Alleenstaand), Geboortedatum, Peildatum,
            BrutoPensioenTotaal, Inkomstenbelasting, NettoPensioenTotaal,
        Tree_3), % (NettoPensioenTotaal wordt verder niet gebruikt )
        % Alvast ingehouden door belastingdienst
        loonheffing2(ref([]),
            aow(Alleenstaand), Geboortedatum, Peildatum,
            AowBedrag, LoonheffingAow, NettoPensioenAow,
        Tree_4),
        % Alvast ingehouden door de pensioenmaatschappijen
        maplist(loonheffing2(ref([]), geen_aow, Geboortedatum, Peildatum),
                PotjesBedragen, LoonheffingPerPotje, NettoPensioenPerPotje, MoreTrees),
        % (het NettoPensioenPerPotje en het NettoPensioenAow is niet nodig om te weten)
        totaal_van(LoonheffingTotaal, [LoonheffingAow|LoonheffingPerPotje], ref([]), Tree_5),
        bedrag_bedrag_sum(LoonheffingTotaal, NogTeBetalenInkomstenbelasting, Inkomstenbelasting, ref([]), Tree_6)
    .
