% -*- Prolog -*-
:- use_module(library(pprint)).

% utils voor herleidbare operaties
%==================================================================================

first_arguments_of_compounds([], []).
first_arguments_of_compounds([A|AL], [C|CL])
    :-
        compound_name_arguments(C, _, [A|_]),
        first_arguments_of_compounds(AL, CL)
    .
values_of_leafs(V, L) :- first_arguments_of_compounds(V, L) .

% misschien is dit niet nodig omdat er (nog) niks gebeurt met losse refs
second_arguments_of_compounds([], []).
second_arguments_of_compounds([A2|AL], [C|CL])
    :-
        compound_name_arguments(C, _, [_, A2|_]),
        second_arguments_of_compounds(AL, CL)
    .
refs_of_leafs(V, L) :- second_arguments_of_compounds(V, L).

% Herleidbare operaties
%=================================================================================

datum_in_jaar(
            leaf(Datum, SubRef1),
            leaf(Jaar, SubRef2),
            Ref, node(
				base(Ref),
                operator("datum_in_jaar", ref([])),
                children([
                    leaf(Datum, SubRef1),
                    leaf(Jaar, SubRef2)
                ])))
    :- datum_in_jaar(Datum, Jaar) .

datum_in_of_na_jaar(
            leaf(Datum, SubRef1),
            leaf(Jaar, SubRef2),
            Ref, node(
				base(Ref),
                operator("datum_in_of_na_jaar", ref([])),
                children([
                    leaf(Datum, SubRef1),
                    leaf(Jaar, SubRef2)
                ])))
    :- datum_in_of_na_jaar(Datum, Jaar) .

eerste_jaar_van(
        LeafJaar,
        LeafJaren,
        Ref, node(
			base(Ref),
            operator("eerste_jaar_van", ref([])),
            children([
                % TODO: nested list ipv concat
                LeafJaar | LeafJaren
            ]))
        ) :-
    % TODO: de herleidbaarheid van LeafJaren gaat hier verloren
    % Onee toch niet want LeafJaren wordt al toegevoegd aan de Tree
    leaf(Jaar, _) = LeafJaar,
    values_of_leafs(Jaren, LeafJaren),
    eerste_jaar_van(Jaar, Jaren).

datum_voor_datum(
            leaf(Datum1, SubRef1),
            leaf(Datum2, SubRef2),
            Ref, node(
				base(Ref),
                operator("datum_voor_datum", ref([])),
                children([
                    leaf(Datum1, SubRef1),
                    leaf(Datum2, SubRef2)
                ])))
    :- datum_voor_datum(Datum1, Datum2) .

datum_na_datum(
            leaf(Datum1, SubRef1),
            leaf(Datum2, SubRef2),
            Ref, node(
				base(Ref),
                operator("datum_na_datum", ref([])),
                children([
                    leaf(Datum1, SubRef1),
                    leaf(Datum2, SubRef2)
                ])))
    :- datum_na_datum(Datum1, Datum2) .

datum_op_of_voor_datum(
            leaf(Datum1, SubRef1),
            leaf(Datum2, SubRef2),
            Ref, node(
				base(Ref),
                operator("datum_op_of_voor_datum", ref([])),
                children([
                    leaf(Datum1, SubRef1),
                    leaf(Datum2, SubRef2)
                ])))
    :- datum_op_of_voor_datum(Datum1, Datum2) .

datum_op_of_na_datum(
            leaf(Datum1, SubRef1),
            leaf(Datum2, SubRef2),
            Ref, node(
				base(Ref),
                operator("datum_op_of_na_datum", ref([])),
                children([
                    leaf(Datum1, SubRef1),
                    leaf(Datum2, SubRef2)
                ])))
    :- datum_op_of_na_datum(Datum1, Datum2) .

dag_voor_datum(
            leaf(Datum1, SubRef1),
            leaf(Datum2, SubRef2),
            Ref, node(
				base(Ref),
                operator("dag_voor_datum", ref([])),
                children([
                    leaf(Datum1, SubRef1),
                    leaf(Datum2, SubRef2)
                ])))
    :- dag_voor_datum(Datum1, Datum2) .

heeft_leeftijd_op_datum(nog_niet_bereikt,
            leaf(Geboortedatum, SubRef1),
            leaf(Leeftijd, SubRef2),
            leaf(Datum, SubRef3),
            Ref, node(
				base(Ref),
                operator("heeft_leeftijd_op_datum nog_niet_bereikt", ref([])),
                children([
                    leaf(Geboortedatum, SubRef1),
                    leaf(Leeftijd, SubRef2),
                    leaf(Datum, SubRef3)
                ])))
    :- heeft_leeftijd_op_datum(nog_niet_bereikt, Geboortedatum, Leeftijd, Datum) .

heeft_leeftijd_op_datum(bereikt,
            leaf(Geboortedatum, SubRef1),
            leaf(Leeftijd, SubRef2),
            leaf(Datum, SubRef3),
            Ref, node(
				base(Ref),
                operator("heeft_leeftijd_op_datum bereikt", ref([])),
                children([
                    leaf(Geboortedatum, SubRef1),
                    leaf(Leeftijd, SubRef2),
                    leaf(Datum, SubRef3)
                ])))
    :- heeft_leeftijd_op_datum(bereikt, Geboortedatum, Leeftijd, Datum) .

bereikt_leeftijd_op_datum(
            leaf(Geboortedatum, SubRef1),
            leaf(Leeftijd, SubRef2),
            leaf(Datum, SubRef3),
            Ref, node(
				base(Ref),
                operator("bereikt_leeftijd_op_datum", ref([])),
                children([
                    leaf(Geboortedatum, SubRef1),
                    leaf(Leeftijd, SubRef2),
                    leaf(Datum, SubRef3)
                ])))
    :- bereikt_leeftijd_op_datum(Geboortedatum, Leeftijd, Datum) .

bedrag_bedrag_sum(
            leaf(A, SubRef1),
            leaf(B, SubRef2),
            leaf(T, SubRef3),
            Ref, node(
				base(Ref),
                operator("bedrag_bedrag_sum", ref([])),
                children([
                    leaf(A, SubRef1),
                    leaf(B, SubRef2),
                    leaf(T, SubRef3)
                ])))
    :- bedrag_bedrag_sum(A, B, T) .

totaal_van(
        LeafTotaal,
        LeafBedragen,
        Ref, node(
			base(Ref),
            operator("totaal_van", ref([])),
            children([
                % TODO: nested list ipv concat?
                LeafTotaal | LeafBedragen
            ]))
        ) :-
    % TODO: de herleidbaarheid van LeafJaren gaat hier verloren
    % Onee toch niet want LeafJaren wordt al toegevoegd aan de Tree
    leaf(Totaal, _) = LeafTotaal,
    values_of_leafs(Bedragen, LeafBedragen),
    totaal_van(Totaal, Bedragen).
