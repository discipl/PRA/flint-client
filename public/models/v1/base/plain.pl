% -*- Prolog -*-
:- use_module(library(clpfd)).
%:- use_module(library(table)).

% Utils voor operaties
%==================================================================================================================

list_min([L|Ls], Min) :- foldl(num_num_min, Ls, L, Min).
num_num_min(X, Y, Min) :- Min #= min(X, Y).

% Operaties
%==================================================================================================================

%aantal_dagen_in_maand_in_jaar(dagen(31), maand(1), _).
%aantal_dagen_in_maand_in_jaar(dagen(28), maand(2), jaar(Y)) :- #\ (Y mod 400 #= 0 #\/ (Y mod 4 #= 0 #/\ Y mod 100 #\= 0)) .
%aantal_dagen_in_maand_in_jaar(dagen(29), maand(2), jaar(Y)) :-     Y mod 400 #= 0 #\/ (Y mod 4 #= 0 #/\ Y mod 100 #\= 0) .
%aantal_dagen_in_maand_in_jaar(dagen(31), maand(3), _).
%aantal_dagen_in_maand_in_jaar(dagen(30), maand(4), _).
%aantal_dagen_in_maand_in_jaar(dagen(31), maand(5), _).
%aantal_dagen_in_maand_in_jaar(dagen(30), maand(6), _).
%aantal_dagen_in_maand_in_jaar(dagen(31), maand(7), _).
%aantal_dagen_in_maand_in_jaar(dagen(31), maand(8), _).
%aantal_dagen_in_maand_in_jaar(dagen(30), maand(9), _).
%aantal_dagen_in_maand_in_jaar(dagen(31), maand(10), _).
%aantal_dagen_in_maand_in_jaar(dagen(30), maand(11), _).
%aantal_dagen_in_maand_in_jaar(dagen(31), maand(12), _).


aantal_dagen_in_maand_in_jaar(dagen(D), maand(M), jaar(Y)) :-
    (    D #= 31 #/\ M in 1 \/ 3 \/ 5 \/ 7 \/ 8 \/ 10 \/ 12)
    #\/ (D #= 30 #/\ M in 4 \/ 6 \/ 9 \/ 11)
    #\/ (D #= 29 #/\ M #= 2 #/\ (Y mod 400 #= 0 #\/ (Y mod 4 #= 0 #/\ Y mod 100 #\= 0)))
    #\/ (D #= 28 #/\ M #= 2 #/\ #\ (Y mod 400 #= 0 #\/ (Y mod 4 #= 0 #/\ Y mod 100 #\= 0))
    ).

gregorian(Y, M, D)
    :-
        Y in -4713..3267,
        M in 1..12,
        (   (D in 1..28)
        #\/ (M #\= 2 #/\ D in 29..30)
        #\/ (M in 1 \/ 3 \/ 5 \/ 7 \/ 8 \/ 10 \/ 12 #/\ D #= 31)
        #\/ (M #= 2 #/\ D #= 29 #/\ Y mod 400 #= 0)
        #\/ (M #= 2 #/\ D #= 29 #/\ Y mod 4 #= 0 #/\ Y mod 100 #\= 0)
        )
    .

eerste_in_domain(datum(Y1,M1,D1), datum(Y,M,D))
    :-
        Y in -4713..3267 #/\
        M in 1..12 #/\
        D in 1..31,
        fd_set(Y, Ys),
        fd_set(M, Ms),
        fd_set(D, Ds),
        fdset_min(Ys, Y1),
        fdset_min(Ms, M1),
        fdset_min(Ds, D1)
    .
        %#/\ Dint in -9999999..9999999
        %datum_intrepr(datum(Y,M,D), Dint),
        %fd_set(Dint, Dint_set),
        %fdset_min(Dint_set, Dint_min),
        %datum_intrepr(datum(Y1,M1,D1), Dint_min),
        %label([Dint_min, Y1, M1, D1])
    %.
%concrete(datum(Y,M,D))
%    :-
%        Y in -4713..3267,
%        M in 1..12,
%        D in 1..31,
%        label([Y,M,D])
%        %label([D,M,Y])
%    .
concrete(datum(Y,M,D))
    :-
        concrete(jaar(Y)),
        concrete(maand(M)),
        concrete(dag(D))
    .
concrete(jaar(Y))
    :-
        Y in -4713..3267,
        indomain(Y)
    .
concrete(maand(M))
    :-
        M in 1..12,
        indomain(M)
    .
concrete(dag(D))
    :-
        D in 1..31,
        indomain(D)
    .
concrete(maanden(M))
    :-
        M in -99999..99999,
        indomain(M)
    .
concrete(dagen(D))
    :-
        D in -3..31,
        indomain(D)
    .
concrete(Leeftijd)
    :-
        leeftijd_in_maanden(Leeftijd, maanden(LeeftijdInMaanden)),
        LeeftijdInMaanden in -99 999..99 999,
        %LeeftijdInMaanden in 0..99 999,
        indomain(LeeftijdInMaanden)
    .
non_negative(leeftijd(Jaren, Maanden))
    :-
        Jaren #>= 0,
        Maanden #>= 0
    .

valide_datum(datum(J,M,D)) :- gregorian(J, M, D) .
datum_intrepr(datum(J,M,D), IntRepr) :- IntRepr #= J*10000 + M*100 + D .

datum_in_jaar(datum(J,_,_), jaar(J)).

datum_in_of_na_jaar(datum(DJ,_,_), jaar(JJ)) :- DJ #>= JJ .

jaar_voor_jaar(jaar(A), jaar(B)) :- A #< B .
jaar_na_jaar(jaar(A), jaar(B)) :- A #> B .

datum_voor_datum(datum(J1,_,_), datum(J2,_,_))
%datum_voor_datum(datum(J1,M1,D1), datum(J2,M2,D2))
    :-
        J1 #< J2
        %,valide_datum(datum(J1,M1,D1))
        %,valide_datum(datum(J2,M2,D2))
    .
datum_voor_datum(datum(J1,M1,_), datum(J2,M2,_))
    :-
        J1 #= J2,
        M1 #< M2
        %,valide_datum(datum(J1,M1,D1))
        %,valide_datum(datum(J2,M2,D2))
    .
datum_voor_datum(datum(J1,M1,D1), datum(J2,M2,D2))
    :-
        J1 #= J2,
        M1 #= M2,
        D1 #< D2
        %,valide_datum(datum(J1,M1,D1))
        %,valide_datum(datum(J2,M2,D2))
    .

datum_na_datum(Datum1, Datum2) :- datum_voor_datum(Datum2, Datum1) .

datum_op_of_na_datum(Datum, Datum).
datum_op_of_na_datum(Datum1, Datum2) :- datum_voor_datum(Datum2, Datum1) .

datum_op_of_voor_datum(Datum, Datum).
datum_op_of_voor_datum(Datum1, Datum2) :- datum_voor_datum(Datum1, Datum2) .

eerste_jaar_van(Min, [L|Ls]) :- foldl(jaar_jaar_min, Ls, L, Min) .
jaar_jaar_min(Jaar, Jaar, Jaar).
jaar_jaar_min(JaarA, JaarB, JaarA) :- jaar_voor_jaar(JaarA, JaarB) .
jaar_jaar_min(JaarA, JaarB, JaarB) :- jaar_na_jaar(JaarA, JaarB) .

eerste_datum_van(Min, [L|Ls]) :- foldl(datum_datum_min, Ls, L, Min) .
datum_datum_min(X, X, X). % Dit is nog niet gebruikt
datum_datum_min(X, Y, X) :- datum_voor_datum(X, Y) .
datum_datum_min(X, Y, Y) :- datum_na_datum(X, Y) .


% varianten die automatisch dispatchen op het juiste data-type
voor(JaarA, JaarB)
    :-
        JaarA = jaar(_),
        JaarB = jaar(_),
        jaar_voor_jaar(JaarA, JaarB)
    .
voor(DatumA, DatumB)
    :-
        DatumA = datum(_, _, _),
        DatumB = datum(_, _, _),
        datum_voor_datum(DatumA, DatumB)
    .
na(JaarA, JaarB)
    :-
        JaarA = jaar(_),
        JaarB = jaar(_),
        jaar_na_jaar(JaarA, JaarB)
    .
na(DatumA, DatumB)
    :-
        DatumA = datum(_, _, _),
        DatumB = datum(_, _, _),
        jaar_na_jaar(DatumA, DatumB)
    .
% TODO? also dispatch on JarenList/DatumList
eerste_van(Jaar, JarenList)
    :-
        Jaar = jaar(_),
        eerste_jaar_van(Jaar, JarenList)
    .
eerste_van(Datum, DatumsList)
    :-
        Datum = datum(_,_,_),
        eerste_datum_van(Datum, DatumsList)
    .

laatste_maand_is_nog_niet_verstreken_op_datum_vanaf_datum_met_restdagen(boolean(1),
            datum(J2,M2,D2), datum(J1,M1,D1), dagen(AantalRestdagen))
    :-
        min(D1, AantalDagenM2) #> D2,
        AantalRestdagen in 1..30 #/\
        AantalRestdagen #= AantalDagenM1-(D1-D2),
        %gregorian(J1,M1,D1),
        %gregorian(J2,M2,D2),
        aantal_dagen_in_maand_in_jaar(dagen(AantalDagenM2), maand(M2), jaar(J2)),
        aantal_dagen_in_maand_in_jaar(dagen(AantalDagenM1), maand(M1), jaar(J1))
    .

laatste_maand_is_nog_niet_verstreken_op_datum_vanaf_datum_met_restdagen(boolean(0),
            datum(J2,M2,D2), datum(_,_,D1), dagen(AantalRestdagen))
            %datum(J2,M2,D2), datum(J1,M1,D1), dagen(AantalRestdagen))
    :-
        min(D1, AantalDagenM2) #=< D2,
        AantalRestdagen in -3..30 #/\
        AantalRestdagen #= D2-D1,
        %gregorian(J1,M1,D1),
        %gregorian(J2,M2,D2),
        aantal_dagen_in_maand_in_jaar(dagen(AantalDagenM2), maand(M2), jaar(J2))
    .

datum_datum_maandenverschil_restdagen(datum(J1,M1,D1), datum(J2,M2,D2), maanden(AantalMaandenverschil), Restdagen)
    :-
        laatste_maand_is_nog_niet_verstreken_op_datum_vanaf_datum_met_restdagen(
            boolean(LaatsteMaandNogNietVerstreken),
            datum(J2,M2,D2),
            datum(J1,M1,D1),
            Restdagen),
        datum_voor_datum(datum(J1,M1,D1), datum(J2,M2,D2)),
        AantalMaandenverschil #= (J2-J1) * 12 + (M2-M1) - LaatsteMaandNogNietVerstreken #/\ M2 in 1..12 #/\ M1 in 1..12
    .

datum_datum_maandenverschil_restdagen(Datum1, Datum2, maanden(NegatiefAantalMaandenverschil), dagen(NegatiefAantalRestdagen))
    :-
        datum_na_datum(Datum1, Datum2),
        datum_datum_maandenverschil_restdagen(Datum2, Datum1, maanden(AantalMaandenverschil), dagen(AantalRestdagen)),
        NegatiefAantalMaandenverschil #= -AantalMaandenverschil,
        NegatiefAantalRestdagen #= -AantalRestdagen
    .

dag_voor_datum(datum(JV,12,31), datum(J,1,1)) :- JV #= J-1 .
dag_voor_datum(datum(J,MV,DV), datum(J,M,1))
    :-
        M #> 1, MV #= M-1,
        aantal_dagen_in_maand_in_jaar(dagen(DV), maand(MV), jaar(J))
    .
dag_voor_datum(datum(J,M,DV), datum(J,M,D)) :-
        D #> 1 #/\ D #=< _dT #/\ DV #= D-1,
        aantal_dagen_in_maand_in_jaar(dagen(_dT), maand(M), jaar(J))
    .

datum_datum_dagenverschil(Datum, Datum, dagen(0)).
datum_datum_dagenverschil(DatumA, DatumB, dagen(Dagen))
    :-
        DagenMin1 #= Dagen - 1,
        dag_voor_datum(DatumVoorDatumB, DatumB),
        datum_datum_dagenverschil(DatumA, DatumVoorDatumB, dagen(DagenMin1)),
        datum_voor_datum(DatumA, DatumB)
    .

leeftijden_tot([], leeftijd(0,0)).
leeftijden_tot([LeeftijdVoorLeeftijd|LeeftijdenRest], Leeftijd)
    :-
        leeftijd_in_maanden(Leeftijd, maanden(AantalMaandentotaal)),
        AantalMaandentotaal #> 0 #/\ AantalMaandentotaalVoor #= AantalMaandentotaal-1,
        leeftijd_in_maanden(LeeftijdVoorLeeftijd, maanden(AantalMaandentotaalVoor)),
        leeftijden_tot(LeeftijdenRest, LeeftijdVoorLeeftijd)
        %,!
    .
datums_van_tot([], Datum, Datum).
datums_van_tot([DatumVoorTot|Rest], DatumVan, DatumTot)
    :-
        datum_voor_datum(DatumVan, DatumTot),
        dag_voor_datum(DatumVoorTot, DatumTot),
        datums_van_tot(Rest, DatumVan, DatumVoorTot)
        %,!
    .

map_datums_van_tot(_, Datum, Datum).
map_datums_van_tot(Goal, DatumVan, DatumTot)
    :-
        datum_voor_datum(DatumVan, DatumTot),
        dag_voor_datum(DatumVoorTot, DatumTot),
        call(Goal, DatumVoorTot),
        map_datums_van_tot(Goal, DatumVan, DatumVoorTot)
    .

map_tot_leeftijd_voor_datum(_, leeftijd(0,0), _).
map_tot_leeftijd_voor_datum(Goal, TotLeeftijd, VoorDatum)
    :-
        leeftijd_in_maanden(TotLeeftijd, maanden(_aantalMaandentotaal)),
        _aantalMaandentotaal #> 0 #/\ _aantalMaandentotaalVoor #= _aantalMaandentotaal-1,
        leeftijd_in_maanden(LeeftijdVoorLeeftijdTot, maanden(_aantalMaandentotaalVoor)),
        datum_voor_datum(_datumVoorDatum, VoorDatum),
        call(Goal, LeeftijdVoorLeeftijdTot, _datumVoorDatum),
        map_tot_leeftijd_voor_datum(Goal, LeeftijdVoorLeeftijdTot, VoorDatum)
    .

leeftijd_in_maanden(leeftijd(AantalJaren, AantalMaanden), maanden(AantalMaandentotaal))
    :-
        AantalJaren #= AantalMaandentotaal // 12 #/\
        AantalMaanden #= AantalMaandentotaal rem 12
    .
leeftijd_korter_dan_leeftijd(L1, L2)
    :-
        leeftijd_in_maanden(L1, maanden(M1)),
        leeftijd_in_maanden(L2, maanden(M2)),
        M1 #< M2
    .
leeftijd_langer_dan_leeftijd(L1, L2)
    :-
        leeftijd_in_maanden(L1, maanden(M1)),
        leeftijd_in_maanden(L2, maanden(M2)),
        M1 #> M2
    .
leeftijd_leeftijd_kortste(L, L, L).
leeftijd_leeftijd_kortste(L1, L2, L1) :- leeftijd_korter_dan_leeftijd(L1, L2) .
leeftijd_leeftijd_kortste(L1, L2, L2) :- leeftijd_langer_dan_leeftijd(L1, L2) .
kortste_leeftijd_van(Min, [L|Ls]) :- foldl(leeftijd_leeftijd_kortste, Ls, L, Min) .
leeftijd_leeftijd_langste(L, L, L).
leeftijd_leeftijd_langste(L1, L2, L1) :- leeftijd_langer_dan_leeftijd(L1, L2) .
leeftijd_leeftijd_langste(L1, L2, L2) :- leeftijd_korter_dan_leeftijd(L1, L2) .
langste_leeftijd_van(Min, [L|Ls]) :- foldl(leeftijd_leeftijd_langste, Ls, L, Min) .

heeft_leeftijd_op_datum(bereikt, Geboortedatum, Leeftijd, Datum)
    :-
        LeeftijdMaandentotaal #>= 0,
        DatumsVerschilMaandentotaal #>= LeeftijdMaandentotaal,
        leeftijd_in_maanden(Leeftijd, maanden(LeeftijdMaandentotaal)),
        datum_datum_maandenverschil_restdagen(Geboortedatum, Datum, maanden(DatumsVerschilMaandentotaal), _)
    .
heeft_leeftijd_op_datum(nog_niet_bereikt, Geboortedatum, Leeftijd, Datum)
    :-
        LeeftijdMaandentotaal #>= 0,
        DatumsVerschilMaandentotaal #< LeeftijdMaandentotaal,
        leeftijd_in_maanden(Leeftijd, maanden(LeeftijdMaandentotaal)),
        datum_datum_maandenverschil_restdagen(Geboortedatum, Datum, maanden(DatumsVerschilMaandentotaal), _)
    .

% TODO delete this predicate
heeft_leeftijd_bereikt_op_datum(Geboortedatum, Leeftijd, Datum)
    :-
        MinimaalMaandentotaal #=< MaximaalMaandentotaal,
        datum_datum_maandenverschil_restdagen(Geboortedatum, Datum, maanden(MaximaalMaandentotaal), _),
        leeftijd_in_maanden(Leeftijd, maanden(MinimaalMaandentotaal))
    .

heeft_exacte_leeftijd_op_datum(Geboortedatum, Leeftijd, Datum)
    :-
        datum_datum_maandenverschil_restdagen(Geboortedatum, Datum, Maandentotaal, _),
        leeftijd_in_maanden(Leeftijd, Maandentotaal)
    .

bereikt_leeftijd_op_datum(Geboortedatum, Leeftijd, Datum)
    :-
        AantalRestdagen #=< 0, % kan negatief zijn: wanneer de dag van Geboortedatum niet in de maand van Datum zit
        datum_datum_maandenverschil_restdagen(Geboortedatum, Datum, Maandentotaal, dagen(AantalRestdagen)),
        leeftijd_in_maanden(Leeftijd, Maandentotaal)
    .



finite(gehele_euros(X)) :-
        %X in 0..999 999 999
        X in 0..999 999 999 999 999 999
    .

int_of_n_digits(0, 1).
int_of_n_digits(Int, N)
    :-
        %Int #= 10^(N-1)
        dif(Int, 0),
        N is floor(log10(abs(Int)))+1
    .

% Zet een percentage op naar een fractie,
% TODO maak gebruikt van int_of_n_digits om zodat aantal decmials ook iets anders dan 2 mag zijn
percentage_as_fraction(percentage(P,TwoDecimals), fraction(Amount, DevidedByTenToThePowerOf))
    :-
        DevidedByTenToThePowerOf = 4,
        P #= Amount // 100,
        TwoDecimals #= Amount mod 100
    .

bedrag_bedrag_sum(gehele_euros(A), gehele_euros(B), gehele_euros(T))
    :-
        T #= A + B
    .

%Test:
%
%bedrag_bedrag_sum(gehele_euros(99),gehele_euros(100), gehele_euros(199)).


totaal_van(T, Ls) :- foldl(bedrag_bedrag_sum, Ls, gehele_euros(0), T) .

%Test:
%
%totaal_van(gehele_euros(200), [ gehele_euros(99),
%                                gehele_euros(100),
%                                gehele_euros(1)    ]).

