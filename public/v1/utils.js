let exports = {}

function pri(o) {
    for (const [sr, obj] of Object.entries(o)) {
        console.log(sr, "=", JSON.stringify(obj, undefined, 2))
    }
} ; exports = {pri, ...exports}

async function loadSpecs(modelFunctionalityOptions_config){
    const spec_files = []
    const model_list = []
    for (const opt of modelFunctionalityOptions_config) {
        if (!model_list.includes(opt.model)) {
            spec_files.push(`/models/v1/${opt.model}/spec.json`)
            model_list.push(opt.model)
        }
    }
    const spec_list = await Promise.all(spec_files.map(url =>
        fetch(url).then(response => response.json())
    ))
    const model2spec = {}
    for (let i = 0; i < model_list.length; i++) {
        const model = model_list[i];
        const spec = spec_list[i];
        model2spec[model] = spec
    }
    return model2spec
} ; exports = {loadSpecs, ...exports}

async function loadTexts(texts_config){
    const marked_files = []
    const quotes_files = []
    const object_list = []
    for (const [filename, obj] of Object.entries(texts_config)) {
        for (const year of obj.years) {
            marked_files.push(`/v1/marked/${year}/${filename}.html`)
            quotes_files.push(`/v1/quotes/${year}/${filename}.json`)
            object_list.push({
                label: `${obj.displayName} (${year})`,
                year,
                filename,
            })
        }
    }
    const [marked_list, quotes_list] = await Promise.all([
        Promise.all(marked_files.map(url =>
            fetch(url).then(response => response.text())
        )),
        Promise.all(quotes_files.map(url =>
            fetch(url).then(response => response.json())
        ))
    ]);

    const fileObjects = {}
    const className2file = {}
    for (let i = 0; i < object_list.length; i++) {
        const marked = marked_list[i];
        const quotes = quotes_list[i];
        const object = object_list[i];
        if (!fileObjects[object.year]) { fileObjects[object.year] = {} }
        fileObjects[object.year][object.filename] = { ...object, marked, quotes, }
        const name = `${object.year}_${object.filename}`
        const year = object.year
        const filename = object.filename
        for (const className of Object.keys(quotes)) {
            className2file[className] = { name, year, filename }
        }
    }
    return [fileObjects, className2file]
}
exports = {...exports, loadTexts}

function refs2quotesStr(className_list, className2file, fileObjects) {
    const quotes = []
    for (const className of className_list) {
        const file = className2file[className]
        const fileQuotes = fileObjects[file.year][file.filename]['quotes']
        const quote = fileQuotes[className].map(x=>`<mark>${x}</mark>`).join(" ... ")
        quotes.push(quote)
    }
    return quotes.join(" ... ").trim()
} ; exports = {refs2quotesStr, ...exports}

function treeFlatMap(tree, fn) {
    if (tree.children)
        return fn(tree).concat( tree.children.map(child => treeFlatMap(child, fn)).flat() )
    else
        return fn(tree)
} ; exports = {treeFlatMap, ...exports}


export default {...exports}
