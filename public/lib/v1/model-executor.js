import { SWIPL } from "https://SWI-Prolog.github.io/npm-swipl-wasm/3/7/8/dynamic-import.js";
// docs at: https://www.swi-prolog.org/pldoc/man?section=wasm-calling

function jsDate2prologDate(jsDate){
    const [year, month, day] = jsDate.split("-")
    return `datum(${year},${month},${day})`
}
function jsInt2prologJaar(x){
    return `jaar(${x})`
}
function jsBool2prologAlleenstaand(x){
    return x ? "alleenstaand" : "samenwonend"
}
function jsInt2prologGeheleEuros(x){
    return `gehele_euros(${x})`
}
const jsValue2prologValue = {
    // jsValue: "YYYY-MM-DD", prologValue: "datum(YYYY,MM,DD)"
     Geboortedatum: jsDate2prologDate,
           Vandaag: jsDate2prologDate,
      Kalenderjaar: jsInt2prologJaar,
      Alleenstaand: jsBool2prologAlleenstaand,
         AowBedrag: jsInt2prologGeheleEuros,
    PotjesBedragen: (x) => x.split(" ").map(jsInt2prologGeheleEuros),
}
function plainInputPrologDef(varName, varValue){
    const value = jsValue2prologValue[varName](varValue)
    if (Array.isArray(value))
        return `${varName} = [${value.join(",")}]`
    else
        return `${varName} = ${value}`
}
function makeTraceableLeaf(value, ref){
    return `leaf(${value}, ${ref})`
}
function traceableInputPrologDef(varName, varValueObj){
    const value = jsValue2prologValue[varName](varValueObj.value)
    const ref = `userdata("${varValueObj.source}", "${varValueObj.reason}")`
    if (Array.isArray(value))
        return `${varName} = [${value.map(x=>makeTraceableLeaf(x, ref)).join(",")}]`
    else
        return `${varName} = ${makeTraceableLeaf(value, ref)}`
}
//TODO vind hier een generieke oplossingen voor: 
//     eindigiend op PerPotje moeten even lange lijsten van leafs zijn als PotjesBedragen
function traceableOutputPrologDef(varName){
    //return `${varName} = leaf(_, ref('${varValueObj.destination}', "${varValueObj.reason}"))`
    //return `${varName} = leaf(_, result("${varName}", _))`
    if (varName.endsWith('PerPotje'))
        return `${varName} = _`
    else
        return `${varName} = leaf(_, result("${varName}", _))`
}

function loadPrologFile(path){
    // TODO: somehow swipl.prolog.consult can not find the files.. for now the files are first
    // read as string and then loaded
    // swipl.prolog.consult(path)
    return fetch(path).then(r=>r.text()).then(t => swipl.prolog.load_string(t))
}

// This is exported because it needs to be called manually when using the blocking API
export async function ensureLoaded(modelPath) {
    if (!(modelPath in modelSpecs)) {
        const spec = await fetch(baseUrl+modelPath+"/spec.json").then(x => x.json())
        modelSpecs[modelPath] = spec
        for (const mp of spec.dependencies) {
            await ensureLoaded(mp)
        }
        // Prolog files can not be loaded async, need to be loaded one by one
        await loadPrologFile(baseUrl+modelPath+"/plain_data.pl")
        await loadPrologFile(baseUrl+modelPath+"/plain.pl")
        await loadPrologFile(baseUrl+modelPath+"/traceable_data.pl")
        await loadPrologFile(baseUrl+modelPath+"/traceable.pl")
        await loadPrologFile(baseUrl+modelPath+"/functionalities.pl")
        console.log(modelPath+ " loaded.")
    }
}
function realizePlainInterface(fnSpec) {
    return `${fnSpec.name}(${fnSpec.args.join(", ")})`
}
function realizeTraceableInterface(fnSpec) {
    return `${fnSpec.name}(${fnSpec.args.join(", ")}, Herleidbaarheid)`
}
/* example:
plainBlocking("algemene-ouderdomswet", "notificatie", {
        Geboortedatum: "1957-05-17",
        Vandaag: "2024-4-30",
    })
*/
export function plainBlocking(modelPath, fnName, inputs) {
    const fnSpec = modelSpecs[modelPath]['functionalities'][fnName]
    const condjunctions = (
        fnSpec.args
            .map(x => fnSpec.inputs.includes(x) ? plainInputPrologDef(x, inputs[x]) : null)
            .filter(x => x !== null)
            .concat(realizePlainInterface(fnSpec))
    )

    const goal = condjunctions.join(",\n") + "."
    console.log(goal)
    return swipl.prolog.query(goal).once()
}
/* example:
traceableBlocking("algemene-ouderdomswet", "notificatie", {
        Geboortedatum: {
            value: "1957-05-17",
            source: "wallet_dateOfBirthOnPID",
            reason: "nodig om te bepalen of de gebruiker een notificatie moet ontvangen",
        },
        Vandaag: {
            value: "2024-4-30",
            source: "clockOnUserDevice",
            reason: "nodig om te bepalen of de nodtificatie op dit moment relevant is",
        },
    })
*/
export function traceableBlocking(modelPath, fnName, inputs) {
    const fnSpec = modelSpecs[modelPath]['functionalities'][fnName]

    const condjunctions = (
        fnSpec.args
            .map(x => fnSpec.inputs.includes(x) ? traceableInputPrologDef(x, inputs[x])
                                                : traceableOutputPrologDef(x))
            .concat(realizeTraceableInterface(fnSpec))
    )

    const goal = condjunctions.join(",\n") + "."
    console.log(goal)
    return swipl.prolog.query(goal).once()
}

export async function plainAsync(modelPath, fnName, inputs) {
    await ensureLoaded(modelPath)
    const fnSpec = modelSpecs[modelPath]['functionalities'][fnName]
    const condjunctions = (
        fnSpec.args
            .map(x => fnSpec.inputs.includes(x) ? plainInputPrologDef(x, inputs[x]) : null)
            .filter(x => x !== null)
            .concat(realizePlainInterface(fnSpec))
    )
    const goal = condjunctions.join(",\n") + "."
    console.log(goal)
    return await swipl.prolog.forEach(goal)
}
export async function traceableAsync(modelPath, fnName, inputs) {
    await ensureLoaded(modelPath)
    const fnSpec = modelSpecs[modelPath]['functionalities'][fnName]
    const condjunctions = (
        fnSpec.args
            .map(x => fnSpec.inputs.includes(x) ? traceableInputPrologDef(x, inputs[x])
                                                : traceableOutputPrologDef(x))
            .concat(realizeTraceableInterface(fnSpec))
    )
    const goal = condjunctions.join(",\n") + "."
    console.log(goal)
    return await swipl.prolog.forEach(goal)
}

export async function plainCallback(modelPath, fnName, inputs, cb) {
    await ensureLoaded(modelPath)
    const fnSpec = modelSpecs[modelPath]['functionalities'][fnName]
    const condjunctions = (
        fnSpec.args
            .map(x => fnSpec.inputs.includes(x) ? plainInputPrologDef(x, inputs[x]) : null)
            .filter(x => x !== null)
            .concat(realizePlainInterface(fnSpec))
    )
    const goal = condjunctions.join(",\n") + "."
    console.log(goal)
    return await swipl.prolog.forEach(goal, cb)
}
export async function traceableCallback(modelPath, fnName, inputs, cb) {
    await ensureLoaded(modelPath)
    const fnSpec = modelSpecs[modelPath]['functionalities'][fnName]
    const condjunctions = (
        fnSpec.args
            .map(x => fnSpec.inputs.includes(x) ? traceableInputPrologDef(x, inputs[x])
                                                : traceableOutputPrologDef(x))
            .concat(realizeTraceableInterface(fnSpec))
    )
    const goal = condjunctions.join(",\n") + "."
    console.log(goal)
    return await swipl.prolog.forEach(goal, cb)
}
let swipl
let baseUrl
let modelSpecs
export async function start(baseUrlArg="//models/v1/") {
    baseUrl = baseUrlArg
    modelSpecs = {}
    swipl = await SWIPL({ arguments: ["-q"] })
    console.log("SWI-Prolog started")
}
